<meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo e(setting('site.title')); ?></title>
    <meta name="description" content="<?php echo e(setting('site.description')); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <?php echo $__env->yieldContent('meta'); ?>
    
    <link rel="shortcut icon" href="<?php echo e(asset('img/favicon.ico')); ?>">

    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/owl.carousel.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/magnific-popup.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/font-awesome.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/themify-icons.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/nice-select.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/flaticon.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/gijgo.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/animate.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/slick.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/slicknav.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('vendor/select2/select2.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('vendor/daterangepicker/daterangepicker.min.css')); ?>" />
    <link rel="stylesheet" href="<?php echo e(asset('vendor/toggle/bootstrap-toggle.min.css')); ?>" />
    
    <!-- <link rel="stylesheet" href="<?php echo e(asset('css/responsive.css')); ?>"> -->
    
    <?php echo $__env->yieldContent('css'); ?><?php /**PATH /var/www/html/providencialis/resources/views/tpl/head.blade.php ENDPATH**/ ?>