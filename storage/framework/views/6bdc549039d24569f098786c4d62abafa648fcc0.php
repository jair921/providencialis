<?php if(Session::has('message')): ?>

<div class="alert alert-<?php echo e(Session::get('alert-type')); ?> alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>	

        <strong><?php echo e(Session::get('message')); ?></strong>

</div>

<?php endif; ?>

<?php if($errors->any()): ?>
<div class="alert alert-danger col-lg-8" role="alert">
    <ul>
        <?php echo implode('', $errors->all('<li>:message</li>')); ?>

    </ul>
</div>
<?php endif; ?><?php /**PATH /var/www/html/providencialis/resources/views/flash-message.blade.php ENDPATH**/ ?>