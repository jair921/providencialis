<?php $__env->startSection('content'); ?>
<!-- slider_area_start -->
<div class="slider_area">
    <div class="single_slider  d-flex align-items-center slider_bg_1">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-7 col-md-6">
                    <div class="slider_text ">
                        <h3 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay=".1s" ><span class="bold">Crea</span> An&aacute;lisis Jurisprudenciales,<br> <span class="bold">Gu&aacute;rdalos</span>, <span class="bold">Organ&iacute;zalos</span> como quieras
                            y <span class="bold">Comp&aacute;rtelos</span>  con tus amigos</h3>
                        <p class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".1s">Provicencialis te permitir&aacute; ser m&aacute;s eficiente a la hora de hacer b&uacute;squedas y an&aacute;lisis de tus sentencias.</p>
                        <div class="video_service_btn wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".1s">
                            <a href="<?php echo e(route('login')); ?>" class="boxed-btn3">Crea tu primer an&aacute;lisis gratis</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 col-md-6">
                    <div class="phone_thumb wow fadeInDown" data-wow-duration="1.1s" data-wow-delay=".2s">
                        <img src="<?php echo e(asset('img/page/hammer_512.png')); ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- slider_area_end -->
<!-- service_area  -->
<div class="service_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center  wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                    <h3>Tu plataforma en l&iacute;nea para analizar jurisprudencia</h3>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-3">
                <div class="col">
                    <div class=" text-center">
                        <div>
                            <img src="<?php echo e(asset('img/page/hammer_128.png')); ?>" alt="">
                        </div>
                        <h3>CREAR <br>
                            <span class="explain">
                                Podrás crear fichas de análisis para las sentencias que estás estudiando. 
                                Para hacerlo, podrás usar las plantillas de fichas de Providencialis 
                                o crear una ficha personalizada.
                            </span></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3">


                <div class=" text-center">
                    <div class="thumb">
                        <img src="<?php echo e(asset('img/page/sort_64.png')); ?>" alt="" style="width: 128px;">
                    </div>
                    <h3>ORGANIZAR <br> 
                        <span class="explain">
                            Podr&aacute;s asignar etiquetas a cada una de tus fichas de an&aacute;lisis, 
                            as&iacute; podr&aacute;s organizarlas y clasificarlas como quieras.
                        </span>
                    </h3>
                </div>

            </div>

            <div class="col-md-3">


                <div class=" text-center">
                    <div>
                        <img src="<?php echo e(asset('img/page/search_128.png')); ?>" alt="">
                    </div>
                    <h3>BUSCAR <br>
                        <span class="explain">
                            Podr&aacute;s buscar fichas de an&aacute;lisis que t&uacute; has creado 
                            o que tus amigos han compartido en la comunidad.
                        </span>
                    </h3>
                </div>
            </div>

            <div class="col-md-3">
                <div class=" text-center">
                    <div>
                        <img src="<?php echo e(asset('img/page/share_128.png')); ?>" alt="">
                    </div>
                    <h3>COMPARTIR <br>
                        <span class="explain">
                            En la comunidad Providencialis podr&aacute;s compartir fichas de an&aacute;lisis con tus amigos.
                        </span>
                    </h3>
                </div>
            </div>


        </div>


    </div>
</div>

<!--/ service_area  -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('tpl.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/providencialis/resources/views/welcome.blade.php ENDPATH**/ ?>