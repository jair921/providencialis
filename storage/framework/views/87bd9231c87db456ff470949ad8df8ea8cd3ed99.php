<?php $__env->startSection('content'); ?>
    <div class="bradcam_area bradcam_bg_1">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="bradcam_text">
                        <h3><?php echo $title; ?></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="contact-section">
        <div class="container">
            <div class="row">
                <?php echo $__env->make('flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <div class="col-lg-8">
                    <form class="form-contact contact_form" action="<?php echo e(route('voyager.login')); ?>" method="post" id="contactForm" >
                        <?php echo csrf_field(); ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input class="form-control" name="email" id="email" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'" placeholder="email" required="">
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="password">Contrase&ntilde;a</label>
                                    <input class="form-control" name="password" id="password" type="password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Contrase&ntilde;a'" placeholder="Contrase&ntilde;a" required="">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="remember">
                                        <input class="" name="remember" id="remember" type="checkbox" >
                                        Recu&eacute;rdame 
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="rememb">
                                        <a href="<?php echo e(route('password.request')); ?>" class="pull-right">Recuperar contrase&ntilde;a</a>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <button type="submit" class="button button-contactForm boxed-btn">Iniciar sesi&oacute;n</button>
                        </div>
                    </form>
                    <p></p>
                    <div class="form-group mt-3">
                        <a href="<?php echo e(route('register')); ?>">
                            <button type="submit" class="button button-contactForm boxed-btn">Registro de Nuevo usuario</button>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 offset-lg-1"></div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('tpl.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/providencialis/resources/views/login.blade.php ENDPATH**/ ?>