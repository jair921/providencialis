<?php $__env->startSection('page_title', __('voyager::generic.'.(isset($form->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular); ?>

<?php $__env->startSection('css'); ?>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <style type="text/css">
        /* Remove bottom margins */
        .row > [class*=col-].no-bottom-margin {
            margin-bottom: 0;
        }

        /* Toggle Button */
        .toggle.btn {
            box-shadow: 0 5px 9px -3px rgba(0, 0, 0, 0.2);
            border: 1px solid rgba(0, 0, 0, 0.2) !important;
        }

        /* Make Inputs a 'lil more visible */
        select,
        input[type="text"],
        .panel-body .select2-selection {
            border: 1px solid rgba(0, 0, 0, 0.17)
        }

        /* Reorder */
        .dd .dd-placeholder {
            max-height: 60px;
            margin-bottom: 22px;
        }
        .dd h3.panel-title,
        .dd-dragel h3.panel-title {
            padding-left: 55px;
        }
        .dd-dragel .panel-body {
            display: none !important;
        }
        .order-handle {
            z-index: 1;
            position: absolute;
            padding: 20px 15px 19px;
            background: rgba(255,255,255,0.2);
            font-size: 15px;
            color: #fff;
            line-height: 20px;
            box-shadow: inset -2px 0px 2px rgba(0,0,0,0.1);
            cursor: move;
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_header'); ?>
    <h1 class="page-title">
        <i class="<?php echo e($dataType->icon); ?>"></i>
        <?php echo e(__('voyager::generic.'.(isset($form->id) ? 'edit' : 'add')).' '.$dataType->display_name_singular); ?>

    </h1>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content edit-add container-fluid">
        <?php echo $__env->make('voyager::alerts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <div class="row">
            <div class="col-md-4">
                <div class="panel">
                    <?php if(count($errors) > 0): ?>
                        <div class="alert alert-danger">
                            <ul>
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="voyager-info-circled"></i> Form Details</h3>
                    </div> <!-- /.panel-heading -->

                    <div class="panel-body">
                        <form
                            role="form"
                            action="<?php if(isset($form->id)): ?>
                            <?php echo e(route('voyager.'.$dataType->slug.'.update', $form->id)); ?>

                            <?php else: ?>
                            <?php echo e(route('voyager.'.$dataType->slug.'.store')); ?>

                            <?php endif; ?>"
                            method="POST"
                            enctype="multipart/form-data">

                            <?php echo e(csrf_field()); ?>


                            <?php if(isset($form->id)): ?>
                                <?php echo e(method_field("PUT")); ?>

                            <?php endif; ?>
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input name="title" class="form-control" type="text"
                                       <?php if(isset($form->title)): ?> value="<?php echo e($form->title); ?>" <?php endif; ?> required>
                            </div>

                            <?php if(isset($form->id)): ?>
                                <div class="form-group">
                                    <label for="shortcode">Shortcode
                                        <small>(Paste this code into a text field to display the form)</small>
                                    </label>
                                    <input
                                        name="shortcode"
                                        class="form-control"
                                        type="text"
                                        value="<?php echo e("{!" . "! forms($form->id) !" . "!}"); ?>"
                                        readonly
                                        data-select-all-contents
                                    />
                                </div>
                            <?php endif; ?>

                            <div class="form-group">
                                <label for="mailto">Mail To
                                    <small>(Separate multiple with ',')</small>
                                </label>
                                <input
                                    name="mailto"
                                    class="form-control"
                                    type="text"
                                    <?php if(isset($form->mailto)): ?> value="<?php echo e($form->mailto); ?>" <?php endif; ?>
                                    placeholder="<?php echo e(setting('forms.default_to_email')); ?>"
                                />
                            </div>

                            <div class="form-group">
                                <label for="layout">Layout</label>
                                <select class="form-control" name="layout" id="layout">
                                    <?php $__currentLoopData = $layouts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $layout): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option
                                            value="<?php echo e($layout); ?>"
                                            <?php if(isset($form->layout) && $form->layout === $layout): ?>
                                            selected="selected"
                                            <?php endif; ?>
                                        >
                                            <?php echo e(ucwords(str_replace(array('_', '-'), ' ', $layout))); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="email_template">Email Template</label>
                                <select class="form-control" name="email_template" id="email_template">
                                    <?php $__currentLoopData = $emailTemplates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emailTemplate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option
                                            value="<?php echo e($emailTemplate); ?>"
                                            <?php if(isset($form->email_template) && $form->email_template === $emailTemplate): ?>
                                            selected="selected"
                                            <?php endif; ?>
                                        >
                                            <?php echo e(ucwords(str_replace(array('_', '-'), ' ', $emailTemplate))); ?>

                                        </option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="message_success">Success Message</label>
                                <input
                                    name="message_success"
                                    class="form-control"
                                    type="text"
                                    <?php if(!isset($form)): ?> value="Success! Thanks for your enquiry." <?php endif; ?>
                                    <?php if(isset($form->message_success)): ?> value="<?php echo e($form->message_success); ?>" <?php endif; ?>
                                    placeholder="Thanks for your enquiry"
                                />
                            </div>

                            <div class="form-group">
                                <label for="hook">Event Hook
                                    <small>(Fires after form is submitted)</small>
                                </label>
                                <input name="hook" class="form-control" type="text"
                                       <?php if(isset($form->hook)): ?> value="<?php echo e($form->hook); ?>" <?php endif; ?>>
                            </div>

                            <button type="submit" class="btn btn-primary">
                                <?php echo e(__('voyager::generic.'.(isset($form->id) ? 'update' : 'add'))); ?>

                                <?php echo e($dataType->display_name_singular); ?>

                            </button>
                        </form>
                    </div>
                </div>

                <?php if(isset($form)): ?>
                    <div class="panel panel-bordered panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="voyager-plus"></i> Add Field</h3>
                        </div> <!-- /.panel-heading -->

                        <div class="panel-body">
                            <form role="form" action="<?php echo e(route('voyager.inputs.store')); ?>" method="POST"
                                enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>


                                <div class="form-group">
                                    <label for="type">Field Type</label>
                                    <select class="form-control" name="type" id="type">
                                        <option value="">-- Select --</option>
                                        <?php $__currentLoopData = config('voyager-forms.available_inputs'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($key); ?>"><?php echo e($value); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div> <!-- /.form-group -->

                                <input type="hidden" name="form_id" value="<?php echo e($form->id); ?>"/>
                                <button type="submit"
                                        class="btn btn-success btn-sm"><?php echo e(__('voyager::generic.add')); ?></button>
                            </form>
                        </div> <!-- /.panel-body -->
                    </div> <!-- /.panel -->
                <?php endif; ?>
            </div>

            <div class="col-md-8">
                <?php if(isset($form)): ?>
                    <div class="dd">
                        <ol class="dd-list">
                            <?php echo $__env->renderEach('voyager-forms::inputs.edit-add', $form->inputs, 'input'); ?>
                        </ol>
                    </div> <!-- /.dd -->
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            /**
             * Confirm DELETE input
             */
            $("[data-delete-input-btn]").on('click', function (e) {
                e.preventDefault();
                var result = confirm("Are you sure you want to delete this input?");
                if (result) $(this).closest('form').submit();
            });

            /**
             * Select all text on focus
             */
            $("[data-select-all-contents]").on("click", function () {
               $(this).select();
            });

            /**
             * ORDER Inputs
             */
             // Init drag 'n drop
            $('.dd').nestable({ handleClass: 'order-handle', maxDepth: 1 });

            // Close all panels when dragging
            $('.order-handle').on('mousedown', function() { $('.dd').addClass('dd-dragging'); });

            // Fire request when drag complete
            $('.dd').on('change', function (e) {
                // Only when it's a result of drag and drop
                // -- Otherwise this triggers on every form change within .dd
                if ($('.dd').hasClass('dd-dragging')) {
                    // And reopen panels once drag has finished
                    $('.dd').removeClass('dd-dragging');

                    // Post the request
                    $.post('<?php echo e(route('voyager.forms.sort')); ?>', {
                        order: JSON.stringify($('.dd').nestable('serialize')),
                        _token: '<?php echo e(csrf_token()); ?>'
                    }, function (data) {
                        toastr.success("Order saved");
                    });
                }
            });
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('voyager::master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/providencialis/vendor/pvtl/voyager-forms/src/Providers/../../resources/views/forms/edit-add.blade.php ENDPATH**/ ?>