<?php
  $dRow = \App\DataRow::where('data_type_id', 6)->where('field', 'decision_type_id')->first();
  $decisionAbs = \json_decode($dRow->details, true);
    
  $dRow2 = \App\DataRow::where('data_type_id', 6)->where('field', 'authority_id')->first();
  $authority = \json_decode($dRow2->details, true);
?>
<table>
    <thead>
        <tr>
            <th>Nombre de la ficha</th>
            <th>Autoridad Judicial</th>
            <th>Número de la sentencia</th>
            <th>Magistrado ponente</th>
            <th>Tema</th>
            <th>Accionante</th>
            <th>Norma Demandada</th>
            <th>Problema Jurídico</th>
            <th>Respuesta al problema jurídico (Regla)</th>
            <th>Ratio Decidendi (Extractos de la sentencia)</th>
            <th>Obiter Dicta (Extractos de la setencia)</th>
            <th>Tipo de decisión</th>
            <th>Contenido de la decisión</th>
            <th>Fecha de creación</th>
        </tr>
    </thead>
    <tbody>
        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e(strip_tags($row->name)); ?></td>
            <td><?php echo e(strip_tags($authority['options'][$row->authority_id])); ?></td>
            <td><?php echo e(strip_tags($row->judgment)); ?></td>
            <td><?php echo e(strip_tags($row->magistrate)); ?></td>
            <td><?php echo e(strip_tags($row->topic)); ?></td>
            <td><?php echo e(strip_tags($row->actuator)); ?></td>
            <td><?php echo e(strip_tags($row->law)); ?></td>
            <td><?php echo e(strip_tags($row->problem)); ?></td>
            <td><?php echo e(strip_tags($row->answer)); ?></td>
            <td><?php echo e(strip_tags($row->ratio_decidendi)); ?></td>
            <td><?php echo e(strip_tags($row->obiter_dicta)); ?></td>
            <td><?php echo e(strip_tags($decisionAbs['options'][$row->decision_type_id])); ?></td>
            <td><?php echo e(strip_tags($row->content_decision)); ?></td>
            <td><?php echo e(strip_tags($row->created_at)); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table><?php /**PATH /var/www/html/providencialis/resources/views/files/export/excel/abstracto.blade.php ENDPATH**/ ?>