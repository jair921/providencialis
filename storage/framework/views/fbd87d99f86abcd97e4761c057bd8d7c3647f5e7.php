<?php $__env->startSection('content'); ?>
    <div class="bradcam_area bradcam_bg_1">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="bradcam_text">
                        <h3><?php echo e($title); ?></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="contact-section">
        <div class="container">
            <div class="row">
                <?php echo $__env->make('flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <div class="col-lg-8">
                    <form class="form-contact contact_form" action="<?php echo e(route('register-save')); ?>" method="post" id="contactForm" novalidate="novalidate">
                        <?php echo csrf_field(); ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name">Nombre</label>
                                    <input class="form-control" name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nombre'" placeholder="Nombre" required="">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="country">Pa&iacute;s</label>
                                    <select class="form-control" name="country" id="country" required="">
                                        <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($country->id); ?>"><?php echo e($country->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="city">Ciudad</label>
                                    <input class="form-control" name="city" id="city" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Ciudad'" placeholder="Ciudad" required="">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="speciality">Especialidad de derecho</label>
                                    <select class="form-control" name="speciality" id="speciality" required="">
                                        <?php $__currentLoopData = $specialities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $speciality): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($speciality->id); ?>"><?php echo e($speciality->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="email">Correo</label>
                                    <input class="form-control" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Correo'" placeholder="Correo" required="">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="confirm">Confirmar Correo</label>
                                    <input class="form-control" name="confirm" id="confirm" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Confirmar Correo'" placeholder="Confirmar Correo" required="">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="password">Contrase&ntilde;a</label>
                                    <input class="form-control valid" name="password" id="email" type="password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Contraseña'" placeholder="Contrase&ntilde;a" required="">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="policy">
                                        <input class="" name="policy" id="policy" type="checkbox" required="">
                                        He le&iacute;do y acepto a conformidad con <a href="<?php echo e(route('terminos-condiciones')); ?>" target="_blank">manejo de datos personales y t&eacute;rminos y condiciones</a>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <button type="submit" class="button button-contactForm boxed-btn">Registrar</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-3 offset-lg-1"></div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('tpl.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/providencialis/resources/views/register.blade.php ENDPATH**/ ?>