<?php $__env->startSection('content'); ?>
    <div class="bradcam_area bradcam_bg_1">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="bradcam_text">
                        <h3><?php echo $title; ?></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="contact-section">
        <div class="container">
            <div class="row">
                <?php if($errors->any()): ?>
                <div class="alert alert-danger col-lg-8" role="alert">
                    <ul>
                        <?php echo implode('', $errors->all('<li>:message</li>')); ?>

                    </ul>
                </div>
                <?php endif; ?>
                <div class="col-lg-8">
                    <?php echo $__env->make('files.edit-add', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
                <div class="col-lg-3 offset-lg-1"></div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('tpl.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/providencialis/resources/views/fileNew.blade.php ENDPATH**/ ?>