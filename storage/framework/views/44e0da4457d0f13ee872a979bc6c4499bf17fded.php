<?php
    $edit = !is_null($dataTypeContentAbstract->getKey());
    $add  = is_null($dataTypeContentAbstract->getKey());
?>


    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="<?php echo e($edit ? route('updateFile', $dataTypeContentAbstract->getKey()) : route('abstracto.store')); ?>"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        <!-- CSRF TOKEN -->
                        <?php echo e(csrf_field()); ?>


                        <div class="panel-body">

                            

                            <!-- Adding / Editing -->
                            <?php
                                $dataTypeRows = $dataTypeAbstract->{($edit ? 'editRows' : 'addRows' )};
                            ?>

                            <?php $__currentLoopData = $dataTypeRows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <!-- GET THE DISPLAY OPTIONS -->
                                <?php
                                    $display_options = $row->details->display ?? NULL;
                                    if ($dataTypeContentAbstract->{$row->field.'_'.($edit ? 'edit' : 'add')}) {
                                        $dataTypeContentAbstract->{$row->field} = $dataTypeContentAbstract->{$row->field.'_'.($edit ? 'edit' : 'add')};
                                    }
                                ?>
                                <?php if(isset($row->details->legend) && isset($row->details->legend->text)): ?>
                                    <legend class="text-<?php echo e($row->details->legend->align ?? 'center'); ?>" style="background-color: <?php echo e($row->details->legend->bgcolor ?? '#f0f0f0'); ?>;padding: 5px;"><?php echo e($row->details->legend->text); ?></legend>
                                <?php endif; ?>

                                <div class="form-group <?php if($row->type == 'hidden'): ?> hidden <?php endif; ?> col-md-<?php echo e($display_options->width ?? 12); ?> <?php echo e($errors->has($row->field) ? 'has-error' : ''); ?>" <?php if(isset($display_options->id)): ?><?php echo e("id=$display_options->id"); ?><?php endif; ?>>
                                    <?php echo e($row->slugify); ?>

                                    <label class="control-label" for="name"><?php echo e($row->getTranslatedAttribute('display_name')); ?></label>
                                    <?php echo $__env->make('voyager::multilingual.input-hidden-bread-edit-add', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    <?php if(isset($row->details->view)): ?>
                                        <?php echo $__env->make($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContentAbstract->{$row->field}, 'action' => ($edit ? 'edit' : 'add'), 'view' => ($edit ? 'edit' : 'add'), 'options' => $row->details], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    <?php elseif($row->type == 'relationship'): ?>
                                        <?php echo $__env->make('voyager::formfields.relationship', ['options' => $row->details], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    <?php else: ?>
                                        <?php echo app('voyager')->formField($row, $dataType, $dataTypeContent); ?>

                                    <?php endif; ?>

                                    <?php $__currentLoopData = app('voyager')->afterFormFields($row, $dataType, $dataTypeContent); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $after): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php echo $after->handle($row, $dataType, $dataTypeContent); ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($errors->has($row->field)): ?>
                                        <?php $__currentLoopData = $errors->get($row->field); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <span class="help-block"><?php echo e($error); ?></span>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <?php $__env->startSection('submit-buttons'); ?>
                                <button type="submit" class="btn btn-primary save"><?php echo e(__('voyager::generic.save')); ?></button>
                            <?php $__env->stopSection(); ?>
                            <?php echo $__env->yieldContent('submit-buttons'); ?>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="<?php echo e(route('voyager.upload')); ?>" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="<?php echo e($dataTypeAbstract->slug); ?>">
                        <?php echo e(csrf_field()); ?>

                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> <?php echo e(__('voyager::generic.are_you_sure')); ?></h4>
                </div>

                <div class="modal-body">
                    <h4><?php echo e(__('voyager::generic.are_you_sure_delete')); ?> '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo e(__('voyager::generic.cancel')); ?></button>
                    <button type="button" class="btn btn-danger" id="confirm_delete"><?php echo e(__('voyager::generic.delete_confirm')); ?></button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
<?php /**PATH /var/www/html/providencialis/resources/views/files/edit-add-a.blade.php ENDPATH**/ ?>