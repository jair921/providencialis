<table>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Correo Electrónico</th>
            <th>Especialdiad de derecho</th>
            <th>País</th>
            <th>Ciudad</th>
            <th>Fecha creación</th>
        </tr>
    </thead>
    <tbody>
        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($user->name); ?></td>
            <td><?php echo e($user->email); ?></td>
               <?php
                  $s = \App\Specialty::find($user->speciality_id);
                  if($s){
                     $esp = $s->name;
                  }else{
                     $esp = '';
                  }
               ?>
            <td><?php echo e($esp); ?></td>
               <?php
                  $s2 = \App\Country::find($user->country_id);
                  if($s2){
                     $country = $s2->name;
                  }else{
                     $country = '';
                  }
               ?>            
            <td><?php echo e($country); ?></td>
            <td><?php echo e($user->city); ?></td>
            <td><?php echo e($user->created_at); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table><?php /**PATH /var/www/html/providencialis/resources/views/users/export.blade.php ENDPATH**/ ?>