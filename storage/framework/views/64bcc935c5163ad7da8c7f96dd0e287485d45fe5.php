<?php $__env->startSection('content'); ?>
    <?php
      function printStarFiles($avg){
         if($avg == 0){
            for($i=0; $i<5;$i++){
                echo '<span class="fa fa-star star-review" style="font-size: 15px;" data-star="" title="'. number_format($avg, 1) .'"></span>'."\n";
            }
            return;
         }
         if($avg == 5){
            for($i=0; $i<5;$i++){
              echo '<span class="fa fa-star checked star-review" style="font-size: 15px;" data-star="" title="'. number_format($avg, 1) .'"></span>'."\n";
            }
            return;
         }
         $avgInt = (int)$avg;
         $c=0;
         for($i=0; $i<$avgInt;$i++){
            echo '<span class="fa fa-star checked star-review" style="font-size: 15px;" data-star="" title="'. number_format($avg, 1) .'"></span>'."\n";
            $c++;
          }
          if(($avg - $avgInt)>=0.5){
            echo '<span class="fa fa-star-half checked star-review half-star" style="font-size: 15px;" data-star="" title="'. number_format($avg, 1) .'"></span>'."\n";
            for($i=0; $i<5-$c-1; $i++){
              echo '<span class="fa fa-star star-review" style="font-size: 15px;" data-star="" title="'. number_format($avg, 1) .'"></span>'."\n";
            }
          }else{
            echo '<span class="fa fa-star star-review" style="font-size: 15px;" data-star="" title="'. number_format($avg, 1) .'"></span>'."\n";
            for($i=0; $i<5-$c-1; $i++){
              echo '<span class="fa fa-star star-review" style="font-size: 15px;" data-star="" title="'. number_format($avg, 1) .'"></span>'."\n";
            }
          }
      }
    ?>
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <h3><?php echo $title; ?></h3>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="contact-section">
        <div class="container">
            <div class="row">
                <?php echo $__env->make('flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <div class="col-lg-8">
                    <form class="form-contact contact_form" action="<?php echo e(route('searchFile')); ?>" method="get" id="contactForm" >
                        <div class="row">
                            
                           
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="authority">Autoridad Judicial</label>
                                    <select class="form-control" name="authority" id="authority" >
                                        <option value=""></option>
                                        <?php $__currentLoopData = $authorities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $authority): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($authority->id); ?>" <?php echo e($request->authority==$authority->id ? 'selected' : ''); ?>><?php echo e($authority->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="judgment">N&uacute;mero de sentencia</label>
                                    <input type="text" class="form-control" name="judgment" id="judgment" value="<?php echo e($request->judgment); ?>">
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="magistrate">Magistrado Ponente</label>
                                    <input type="text" class="form-control" name="magistrate" id="magistrate" value="<?php echo e($request->magistrate); ?>">
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="search">B&uacute;squeda general</label>
                                    <input type="text" class="form-control" name="search" id="search" value="<?php echo e($request->search); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <button type="submit" class="button button-contactForm boxed-btn">Buscar</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-3 offset-lg-1"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-bordered">
                                <div class="panel-body">

                                    <div class="table-responsive">
                                        <table id="dataTable" class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <a href="">Autoridad Judicial</a>
                                                    </th>
                                                    <th>
                                                        <a href="">N&uacute;mero de sentencia</a>
                                                    </th>
                                                    <th>
                                                        <a href="">Magistrado ponente</a>
                                                        </th>
                                                    <th>
                                                        <a href="">Fecha de creaci&oacute;n</a>
                                                    </th>
                                                    <th>
                                                        <a href="">Valoraci&oacute;n promedio</a>
                                                    </th>
                                                    <th class="actions text-right">Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if(count($query)>0): ?>
                                                    <?php $__currentLoopData = $query; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td>
                                                            <?php if($row->type_control=='c'): ?>
                                                               <?php
                                                                 $at = \App\Authority::find($row->authority_id);
                                                               ?>
                                                               <?php echo e($at->name); ?>

                                                            <?php else: ?>
                                                              <?php echo e($authorityAbs['options'][$row->authority_id]); ?>

                                                            <?php endif; ?>
                                                        </td>
                                                        <td><?php echo e($row->judgment); ?></td>
                                                        <td><?php echo e($row->magistrate); ?></td>
                                                        <td><?php echo e($row->created_at); ?></td>
                                                        <td>
                                                            <?php
                                                              if($row->type_control=='c'){
                                                                $avg = \App\ReviewFile::where('file_id', $row->id)->avg('star');
                                                              }else{
                                                                $avg = \App\ReviewFile::where('file_id', $row->id)->avg('star'); 
                                                              }
                                                            ?>
                                                            <?php echo e(printStarFiles($avg)); ?>

                                                        </td>
                                                        <?php
                                                          if($row->type_control=='c')
                                                             $dataType = $dataType1;
                                                          else
                                                             $dataType = $dataType2;

                                                             $data = $row;
                                                        ?>
                                                        <td class="no-sort no-click bread-actions">
                                                            <?php $__currentLoopData = $actions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $action): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <?php if(!method_exists($action, 'massAction')): ?>
                                                                    <?php
                                                                        // need to recreate object because policy might depend on record data
                                                                        $class = get_class($action);
                                                                        $action = new $class($dataType, $data);
                                                                    ?>
                                                                    <a href="<?php echo e($action->getRoute($dataType->name)); ?>" title="<?php echo e($action->getTitle()); ?>" <?php echo $action->convertAttributesToHtml(); ?>>
                                                                        <i class="<?php echo e($action->getIcon()); ?>"></i> <span class="hidden-xs hidden-sm"><?php echo e($action->getTitle()); ?></span>
                                                                    </a>
                                                                <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php else: ?>
                                                <tr>
                                                    <td colspan="8">No hay fichas para mostrar</td>
                                                </tr>
                                                <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                    <div class="pull-left">
                                        <div role="status" class="show-res" aria-live="polite"><?php echo e(trans_choice(
                                            'voyager::generic.showing_entries', $total, [
                                                'from' => $from,
                                                'to' => $to,
                                                'all' => $total
                                            ])); ?></div>
                                    </div>
                                    <div class="pull-right">
                                        <?php echo e($query->links()); ?>

                                    </div>
                                    
                                </div>
                    </div>
                </div>
            </div>
            <?php if(count($query)>0): ?>
            <div class="row">
                <div class="form-group mt-3">
                    <a href="<?php echo e(route('export.excel').($qExport ? '?'.$qExport : '')); ?>">
                        <button type="submit" class="button button-contactForm boxed-btn">Exportar a Excel</button>
                    </a>
                </div>
                <div class="form-group col-md-1"></div>
                <div class="form-group mt-3">
                    <a href="<?php echo e(route('export.word').($qExport ? '?'.$qExport : '')); ?>">
                        <button type="submit" class="button button-contactForm boxed-btn">Exportar a Word</button>
                    </a>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </section>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        $(document).ready(function(){
            var decisions = <?php echo $jDecision; ?>;
            $("#type").change(function(){
                var val = $(this).val();
                var options="<option value=\"\"></option>";
                if(val == "1"){
                    $.each(decisions.concreto, function(i, v){
                        options+="<option value=\""+i+"\">"+v+"</option>";
                    });
                    $("#typeD").html(options);
                }else if(val == "2"){
                    $.each(decisions.abstracto, function(i, v){
                        options+="<option value=\""+i+"\">"+v+"</option>";
                    });
                    $("#typeD").html(options);
                }else{
                    $.each(decisions.concreto, function(i, v){
                        options+="<option value=\""+i+"\">"+v+"</option>";
                    });
                    $.each(decisions.abstracto, function(i, v){
                        options+="<option value=\""+i+"\">"+v+"</option>";
                    });
                    $("#typeD").html(options);
                }
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('tpl.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/providencialis/resources/views/files/search.blade.php ENDPATH**/ ?>