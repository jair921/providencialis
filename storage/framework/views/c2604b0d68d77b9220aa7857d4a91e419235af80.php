<?php $__env->startSection('content'); ?>
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <h3><?php echo $title; ?></h3>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="contact-section">
    <div class="container">
        <div class="row">
                <div class="col-lg-8">
                    <form class="form-contact contact_form" action="<?php echo e(route('myFiles')); ?>" method="get" id="contactForm" >
                        <div class="row">
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="authority">Autoridad Judicial</label>
                                    <select class="form-control" name="authority" id="authority" >
                                        <option value=""></option>
                                        <?php $__currentLoopData = $authorities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $authority): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($authority->id); ?>" <?php echo e($request->authority==$authority->id ? 'selected' : ''); ?>><?php echo e($authority->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="judgment">N&uacute;mero de sentencia</label>
                                    <input type="text" class="form-control" name="judgment" id="judgment" value="<?php echo e($request->judgment); ?>">
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="magistrate">Magistrado Ponente</label>
                                    <input type="text" class="form-control" name="magistrate" id="magistrate" value="<?php echo e($request->magistrate); ?>">
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="search">B&uacute;squeda general</label>
                                    <input type="text" class="form-control" name="search" id="search" value="<?php echo e($request->search); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <button type="submit" class="button button-contactForm boxed-btn">Buscar</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-3 offset-lg-1"></div>
            </div>
        <div class="row">
            <?php echo $__env->make('flash-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="col-lg-12">
                <div class="page-content browse container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-bordered">
                                <div class="panel-body">

                                    <div class="table-responsive">
                                        <table id="dataTable" class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <a href="">Nombre personalizado de la ficha</a>
                                                    </th>
                                                    <th>
                                                        <a href="">Autoridad Judicial</a>
                                                    </th>
                                                    <th>
                                                        <a href="">N&uacute;mero de sentencia</a>
                                                    </th>
                                                    <th>
                                                        <a href="">Magistrado ponente</a>
                                                    </th>
                                                    <th>
                                                        <a href="">Fecha de creaci&oacute;n</a>
                                                    </th>
                                                    <th>
                                                        <a href="">Visibilidad</a>
                                                    </th>
                                                    <th class="actions text-right">Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if(count($query)>0): ?>
                                                    <?php $__currentLoopData = $query; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <tr>
                                                        <td><?php echo e($row->name); ?></td>
                                                        <td>
                                                            <?php if($row->type_control=='c'): ?>
                                                               <?php
                                                                 $at = \App\Authority::find($row->authority_id);
                                                               ?>
                                                               <?php echo e($at->name); ?>

                                                            <?php else: ?>
                                                              <?php echo e($authorityAbs['options'][$row->authority_id]); ?>

                                                            <?php endif; ?>
                                                        </td>
                                                        <td><?php echo e($row->judgment); ?></td>
                                                        <td><?php echo e($row->magistrate); ?></td>
                                                        <td><?php echo e($row->created_at); ?></td>
                                                        <td><?php echo $row->view=="1" ? 'P&uacute;blico' : 'Privado'; ?></td>
                                                        <?php
                                                          if($row->type_control=='c')
                                                             $dataType = $dataType1;
                                                          else
                                                             $dataType = $dataType2;

                                                             $data = $row;
                                                        ?>
                                                        <td class="no-sort no-click bread-actions">
                                                        <?php $__currentLoopData = $actions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $action): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php if(!method_exists($action, 'massAction')): ?>
                                                                <?php
                                                                    // need to recreate object because policy might depend on record data
                                                                    $class = get_class($action);
                                                                    $action = new $class($dataType, $data);
                                                                ?>
                                                                <a href="<?php echo e($action->getRoute($dataType->name)); ?>" title="<?php echo e($action->getTitle()); ?>" <?php echo $action->convertAttributesToHtml(); ?>>
                                                                    <i class="<?php echo e($action->getIcon()); ?>"></i> <span class="hidden-xs hidden-sm"><?php echo e($action->getTitle()); ?></span>
                                                                </a>
                                                                <?php endif; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </td>
                                                    </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php else: ?>
                                                <tr>
                                                    <td colspan="8">No hay fichas creadas</td>
                                                </tr>
                                                <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                    <div class="pull-left">
                                        <div role="status" class="show-res" aria-live="polite"><?php echo e(trans_choice(
                                            'voyager::generic.showing_entries', $total, [
                                                'from' => $from,
                                                'to' => $to,
                                                'all' => $total
                                            ])); ?></div>
                                    </div>
                                    <div class="pull-right">
                                        <?php echo e($query->links()); ?>

                                    </div>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 offset-lg-1"></div>
        </div>
        <?php if(count($query)>0): ?>
            <div class="row">
                <div class="form-group mt-3">
                    <a href="<?php echo e(route('export.excel').($qExport ? '?'.$qExport : '')); ?>">
                        <button type="submit" class="button button-contactForm boxed-btn">Exportar a Excel</button>
                    </a>
                </div>
                <div class="form-group col-md-1"></div>
                <div class="form-group mt-3">
                    <a href="<?php echo e(route('export.word').($qExport ? '?'.$qExport : '')); ?>">
                        <button type="submit" class="button button-contactForm boxed-btn">Exportar a Word</button>
                    </a>
                </div>
            </div>
            <?php endif; ?>
    </div>
</section>
    <?php if(count($query)): ?>
    
        <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo e(__('voyager::generic.close')); ?>"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="voyager-trash"></i> <?php echo e(__('voyager::generic.delete_question')); ?> <?php echo e(strtolower($dataType->getTranslatedAttribute('display_name_singular'))); ?>?</h4>
                    </div>
                    <div class="modal-footer">
                        <form action="#" id="delete_form" method="POST">

                            <?php echo e(csrf_field()); ?>

                            <input type="submit" class="btn btn-danger pull-right delete-confirm" value="<?php echo e(__('voyager::generic.delete_confirm')); ?>">
                        </form>
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><?php echo e(__('voyager::generic.cancel')); ?></button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = $(this).data("url");
            $('#delete_modal').modal('show');
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('tpl.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/providencialis/resources/views/files/index.blade.php ENDPATH**/ ?>