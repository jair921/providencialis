<?php $__env->startSection('css'); ?>
    <?php
      function percentStarFiles($stars, $star){
         if($stars == 0) return 0;
         return number_format(($star*100)/$stars, 0);
      }
    ?>
<style>
    /* Individual bars */
    .bar-5 {width: <?php echo e(percentStarFiles($stars, $star5)); ?>%; height: 18px; background-color: #4CAF50;}
    .bar-4 {width: <?php echo e(percentStarFiles($stars, $star4)); ?>%; height: 18px; background-color: #2196F3;}
    .bar-3 {width: <?php echo e(percentStarFiles($stars, $star3)); ?>%; height: 18px; background-color: #00bcd4;}
    .bar-2 {width: <?php echo e(percentStarFiles($stars, $star2)); ?>%; height: 18px; background-color: #ff9800;}
    .bar-1 {width: <?php echo e(percentStarFiles($stars, $star1)); ?>%; height: 18px; background-color: #f44336;}
    <?php if(auth()->guard()->check()): ?>
    #rates:hover > span:before {
        color: orange;
    }
    #rates > .star-review:hover ~ .star-review:before {
        color: #c7c5c5;
    }
    <?php endif; ?>
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php
      function printStarFiles($avg){
         if($avg == 0){
            for($i=0; $i<5;$i++){
                echo '<span class="fa fa-star star-review" style="font-size: 25px;" data-star=""></span>'."\n";
            }
            return;
         }
         if($avg == 5){
            for($i=0; $i<5;$i++){
              echo '<span class="fa fa-star checked star-review" style="font-size: 25px;" data-star=""></span>'."\n";
            }
            return;
         }
         $avgInt = (int)$avg;
         $c=0;
         for($i=0; $i<$avgInt;$i++){
            echo '<span class="fa fa-star checked star-review" style="font-size: 25px;" data-star=""></span>'."\n";
            $c++;
          }
          if(($avg - $avgInt)>=0.5){
            echo '<span class="fa fa-star-half checked star-review half-star" style="font-size: 25px;" data-star=""></span>'."\n";
            for($i=0; $i<5-$c-1; $i++){
              echo '<span class="fa fa-star star-review" style="font-size: 25px;" data-star=""></span>'."\n";
            }
          }else{
            echo '<span class="fa fa-star star-review" style="font-size: 25px;" data-star=""></span>'."\n";
            for($i=0; $i<5-$c-1; $i++){
              echo '<span class="fa fa-star star-review" style="font-size: 25px;" data-star=""></span>'."\n";
            }
          }
      }
    ?>
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <h3><?php echo $title; ?></h3>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <div class="page-content read container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group mt-3">
                                <a href="#" onclick="window.history.back()">
                                    <button type="submit" class="button button-contactForm boxed-btn">Regresar</button>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-12" id="rates">
                            <span class="heading">Calificaciones</span>
                            <?php echo e(printStarFiles($avg)); ?>

                            <p><?php echo e(number_format($avg, 1)); ?> promedio  basado en <?php echo e($stars); ?> calificaciones. <a href="#" id="viewDetail">Ver detalle</a></p>
                        </div>
                        
                        <div class="col-md-12" style="display:none;" id="detail">
                            <div class="panel panel-bordered">
                                <div class="side">
                                  <div>5 estrellas</div>
                                </div>
                                <div class="middle">
                                  <div class="bar-container">
                                    <div class="bar-5"></div>
                                  </div>
                                </div>
                                <div class="side right">
                                  <div><?php echo e($star5); ?></div>
                                </div>
                                <div class="side">
                                  <div>4 estrellas</div>
                                </div>
                                <div class="middle">
                                  <div class="bar-container">
                                    <div class="bar-4"></div>
                                  </div>
                                </div>
                                <div class="side right">
                                  <div><?php echo e($star4); ?></div>
                                </div>
                                <div class="side">
                                  <div>3 estrella</div>
                                </div>
                                <div class="middle">
                                  <div class="bar-container">
                                    <div class="bar-3"></div>
                                  </div>
                                </div>
                                <div class="side right">
                                  <div><?php echo e($star3); ?></div>
                                </div>
                                <div class="side">
                                  <div>2 estrellas</div>
                                </div>
                                <div class="middle">
                                  <div class="bar-container">
                                    <div class="bar-2"></div>
                                  </div>
                                </div>
                                <div class="side right">
                                  <div><?php echo e($star2); ?></div>
                                </div>
                                <div class="side">
                                  <div>1 estrella</div>
                                </div>
                                <div class="middle">
                                  <div class="bar-container">
                                    <div class="bar-1"></div>
                                  </div>
                                </div>
                                <div class="side right">
                                  <div><?php echo e($star1); ?></div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12">

                            <div class="panel panel-bordered" style="padding-bottom:5px;">
                                <!-- form start -->
                                <?php $__currentLoopData = $dataType->readRows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php
                                if ($dataTypeContent->{$row->field.'_read'}) {
                                $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_read'};
                                }
                                ?>
                                <div class="panel-heading" style="border-bottom:0;">
                                    <h3 class="panel-title"><?php echo e($row->getTranslatedAttribute('display_name')); ?></h3>
                                </div>

                                <div class="panel-body" style="padding-top:0;">
                                    <?php if(isset($row->details->view)): ?>
                                    <?php echo $__env->make($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => 'read', 'view' => 'read', 'options' => $row->details], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    <?php elseif($row->type == "image"): ?>
                                    <img class="img-responsive"
                                         src="<?php echo e(filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field})); ?>">
                                    <?php elseif($row->type == 'multiple_images'): ?>
                                    <?php if(json_decode($dataTypeContent->{$row->field})): ?>
                                    <?php $__currentLoopData = json_decode($dataTypeContent->{$row->field}); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <img class="img-responsive"
                                         src="<?php echo e(filter_var($file, FILTER_VALIDATE_URL) ? $file : Voyager::image($file)); ?>">
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                    <img class="img-responsive"
                                         src="<?php echo e(filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field})); ?>">
                                    <?php endif; ?>
                                    <?php elseif($row->type == 'relationship'): ?>
                                    <?php echo $__env->make('voyager::formfields.relationship', ['view' => 'read', 'options' => $row->details], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    <?php elseif($row->type == 'select_dropdown' && property_exists($row->details, 'options') &&
                                    !empty($row->details->options->{$dataTypeContent->{$row->field}})
                                    ): ?>
                                    <?php echo $row->details->options->{$dataTypeContent->{$row->field}}; ?>
                                    <?php elseif($row->type == 'select_multiple'): ?>
                                    <?php if(property_exists($row->details, 'relationship')): ?>

                                    <?php $__currentLoopData = json_decode($dataTypeContent->{$row->field}); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php echo e($item->{$row->field}); ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    <?php elseif(property_exists($row->details, 'options')): ?>
                                    <?php if(!empty(json_decode($dataTypeContent->{$row->field}))): ?>
                                    <?php $__currentLoopData = json_decode($dataTypeContent->{$row->field}); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if(@$row->details->options->{$item}): ?>
                                    <?php echo e($row->details->options->{$item} . (!$loop->last ? ', ' : '')); ?>

                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                    <?php echo e(__('voyager::generic.none')); ?>

                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php elseif($row->type == 'date' || $row->type == 'timestamp'): ?>
                                    <?php if( property_exists($row->details, 'format') && !is_null($dataTypeContent->{$row->field}) ): ?>
                                    <?php echo e(\Carbon\Carbon::parse($dataTypeContent->{$row->field})->formatLocalized($row->details->format)); ?>

                                    <?php else: ?>
                                    <?php echo e($dataTypeContent->{$row->field}); ?>

                                    <?php endif; ?>
                                    <?php elseif($row->type == 'checkbox'): ?>
                                    <?php if(property_exists($row->details, 'on') && property_exists($row->details, 'off')): ?>
                                    <?php if($dataTypeContent->{$row->field}): ?>
                                    <span class="label label-info"><?php echo e($row->details->on); ?></span>
                                    <?php else: ?>
                                    <span class="label label-primary"><?php echo e($row->details->off); ?></span>
                                    <?php endif; ?>
                                    <?php else: ?>
                                    <?php echo e($dataTypeContent->{$row->field}); ?>

                                    <?php endif; ?>
                                    <?php elseif($row->type == 'color'): ?>
                                    <span class="badge badge-lg" style="background-color: <?php echo e($dataTypeContent->{$row->field}); ?>"><?php echo e($dataTypeContent->{$row->field}); ?></span>
                                    <?php elseif($row->type == 'coordinates'): ?>
                                    <?php echo $__env->make('voyager::partials.coordinates', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    <?php elseif($row->type == 'rich_text_box'): ?>
                                    <?php echo $__env->make('voyager::multilingual.input-hidden-bread-read', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    <?php echo $dataTypeContent->{$row->field}; ?>

                                    <?php elseif($row->type == 'file'): ?>
                                    <?php if(json_decode($dataTypeContent->{$row->field})): ?>
                                    <?php $__currentLoopData = json_decode($dataTypeContent->{$row->field}); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <a href="<?php echo e(Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: ''); ?>">
                                        <?php echo e($file->original_name ?: ''); ?>

                                    </a>
                                    <br/>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                    <a href="<?php echo e(Storage::disk(config('voyager.storage.disk'))->url($row->field) ?: ''); ?>">
                                        <?php echo e(__('voyager::generic.download')); ?>

                                    </a>
                                    <?php endif; ?>
                                    <?php else: ?>
                                    <?php echo $__env->make('voyager::multilingual.input-hidden-bread-read', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    <p><?php echo e($dataTypeContent->{$row->field}); ?></p>
                                    <?php endif; ?>
                                </div><!-- panel-body -->
                                <?php if(!$loop->last): ?>
                                <hr style="margin:0;">
                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-3 offset-lg-1"></div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
    $(document).ready(function(){
        $("#viewDetail").click(function(){
            
            $("#detail").toggle("slow");
            
            return false;
        });
        
        <?php if(auth()->guard()->check()): ?>
        $(".half-star").hover(function(){
            $(this).addClass("fa-star");
            $(this).removeClass("fa-star-half");
        }, function(){
            $(this).addClass("fa-star-half");
            $(this).removeClass("fa-star");
        });
        
        var star=1;
        $(".star-review").each(function(){
            $(this).data("star", star);
            star++;
        });
        $(".star-review").click(function(){
            var review = $(this).data("star");
            <?php
              if($dataTypeContent->type_control == "c"){
                  $url = route("starC");
              }else{
                  $url = route("starA");
              }
            ?>
            $.get("<?php echo e($url); ?>", {star:review, id:<?php echo e($dataTypeContent->id); ?>}, function(e){
                window.location.reload();
            });
        });
        <?php endif; ?>
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('tpl.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/providencialis/resources/views/files/read.blade.php ENDPATH**/ ?>