<li class="dd-item" data-id="<?php echo e($input->id); ?>" id="input-id-<?php echo e($input->id); ?>">
    <i class="glyphicon glyphicon-sort order-handle"></i>
    <div class="panel panel-bordered panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo e(ucwords(str_replace('_', ' ', $input->type))); ?> Input</h3>
        </div> <!-- /.panel-heading -->

        <div class="panel-body">
            <form role="form" action="<?php echo e(route('voyager.inputs.update', $input->id)); ?>"
                    method="POST"
                    enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <?php echo e(method_field("PUT")); ?>


                <div class="row">
                    <div class="col-md-4 no-bottom-margin">
                        <div class="form-group">
                            <label for="label">Type</label>
                            <select class="form-control" name="type" id="type" required>
                                <?php $__currentLoopData = config('voyager-forms.available_inputs'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php echo e($key); ?>

                                    <option value="<?php echo e($key); ?>" <?php if($key === $input->type): ?> selected <?php endif; ?>><?php echo e($value); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4 no-bottom-margin">
                        <div class="form-group">
                            <label for="label">Label</label>
                            <input name="label" class="form-control" id="label" value="<?php echo e($input->label); ?>" required>
                        </div>
                    </div>

                    <?php if(in_array($input->type, ['checkbox', 'select', 'radio', 'file'])): ?>
                        <div class="col-md-4 no-bottom-margin">
                            <div class="form-group">
                                <?php if($input->type !== 'file'): ?>
                                    <label for="options">Options <small>(Separated with ", ")</small></label>
                                <?php else: ?>
                                    <label for="options">MIME types <small>(Eg. ".pdf,.zip")</small></label>
                                <?php endif; ?>
                                <input name="options" class="form-control" id="options" value="<?php echo e($input->options); ?>"
                                        required>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="col-md-4 no-bottom-margin">
                        <div class="form-group">
                            <label for="class">CSS Classes</label>
                            <input name="class" class="form-control" id="class" value="<?php echo e($input->class); ?>">
                        </div>
                    </div>

                    <div class="col-md-4 no-bottom-margin">
                        <div class="form-group">
                            <input
                                type="checkbox"
                                name="required"
                                id="required"
                                data-name="required"
                                class="toggleswitch"
                                value="1"
                                data-on="Yes" <?php echo e($input->required ? 'checked="checked"' : ''); ?>

                                data-off="No"
                            />
                            <label for="required"> &nbsp;Input Required</label>
                        </div> <!-- /.form-group -->
                    </div>
                </div> <!-- /.row -->

                <input type="hidden" name="input_id" value="<?php echo e($input->id); ?>"/>
                <button type="submit"
                        style="float:left"
                        class="btn btn-success btn-sm"><?php echo e(__('Update This Input')); ?></button>
            </form>

            <form method="POST" action="<?php echo e(route('voyager.inputs.destroy', $input->id)); ?>">
                <?php echo e(method_field("DELETE")); ?>

                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                <span class="btn-group-xs">
                    <button
                        data-delete-input-btn
                        type="submit"
                        style="float:right; margin-top:12px"
                        class="btn btn-danger btn-xs delete"
                    ><?php echo e(__('voyager::generic.delete')); ?> This Input</button>
                </span>
            </form>
        </div> <!-- /.panel-body -->
    </div> <!-- /.panel -->
</li>
<?php /**PATH /var/www/html/providencialis/vendor/pvtl/voyager-forms/src/Providers/../../resources/views/inputs/edit-add.blade.php ENDPATH**/ ?>