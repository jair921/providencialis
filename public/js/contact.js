$(document).ready(function(){
    
    (function($) {
        "use strict";

    
    jQuery.validator.addMethod('answercheck', function (value, element) {
        return this.optional(element) || /^\bcat\b$/.test(value)
    }, "type the correct answer -_-");

    // validate contactForm form
    $(function() {
        $('#contactForm').validate({
            lang: 'es',
            rules: {
                name: {
                    required: true,
                },
                country: {
                    required: true,
                },
                city: {
                    required: true,
                },
                speciality: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true
                },
                confirm: {
                    required: true,
                    email: true,
                    equalTo:'#email'
                },
                password: {
                    required: true                    
                },
                policy: {
                    required: true
                }
            }
        });
    })
        
 })(jQuery)
})