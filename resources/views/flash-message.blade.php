@if (Session::has('message'))

<div class="alert alert-{{Session::get('alert-type')}} alert-block">

	<button type="button" class="close" data-dismiss="alert">×</button>	

        <strong>{{ Session::get('message') }}</strong>

</div>

@endif

@if($errors->any())
<div class="alert alert-danger col-lg-8" role="alert">
    <ul>
        {!! implode('', $errors->all('<li>:message</li>')) !!}
    </ul>
</div>
@endif