<!doctype html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    @include('tpl.head')
</head>

<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
<!-- header-start -->
    <header>
        <div class="header-area ">
            <div id="sticky-header" class="main-header-area">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-xl-3 col-lg-2">
                            <div class="logo ">
                                <a href="{{route('index')}}">
                                    <span class="logoName">Providencialis</span> Colombia
                                </a>
                                <img src="{{asset('img/page/colombia.png')}}" alt="" class="flag">
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-7">
                            <div class="main-menu  d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a class="active" href="{{route('index')}}">Inicio</a></li>
                                        @guest
                                          <li><a href="{{route('register')}}">Registro de Nuevo usuario</a></li>
                                        @endguest
                                        <li><a href="{{route('searchFile')}}">Buscar Fichas</a></li>
                                        @auth
                                            <li>
                                                <a href="">Mis Fichas</a>
                                                <ul class="submenu">
                                                    <li><a href="{{route('newFile')}}">Crear nuevo an&aacute;lisis</a></li>
                                                    <li><a href="{{route('myFiles')}}">Fichas creadas</a></li>
                                                </ul>
                                            </li>
                                        @endauth
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        @guest
                        <div class="col-xl-3 col-lg-3 d-lg-block d-none">
                            <div class="Appointment">
                                <div class="book_btn d-none d-lg-block">
                                    <a data-toggle="popover" href="#">Iniciar sesi&oacute;n</a>
                                </div>
                            </div>
                            <div id="popover-content" class="hide" style="display: none;">
                                <form role="form" action="{{ route('voyager.login') }}" method="post">
                                    @csrf
                                  <div class="form-group">
                                      <input placeholder="Email" class="form-control" type="email" required="" name="email">
                                      <input placeholder="Contrase&ntilde;a" class="form-control" type="password" required="" name="password">
                                      <input type="hidden" name="remember" id="remember" value="1">
                                      <button type="submit" class="btn btn-primary" id="loginOrSignupButton">Ingresar</button><br>
                                      <a href="{{route('password.request')}}" class="pull-right">Recuperar contrase&ntilde;a</a><br>
                                      <a href="{{route('register')}}" class="pull-right">Registrate como Nuevo usuario</a><br>
                                  </div>
                                </form>
                            </div>
                        </div>
                        @endguest
                        @auth
                           <div class="col-xl-3 col-lg-3 d-lg-block d-none">
                                <div class="Appointment">
                                    <div class="book_btn d-none d-lg-block">
                                        <a href="{{route('voyager.logoutG2')}}">Cerrar sesi&oacute;n</a>
                                    </div>
                                </div>
                            </div>
                        @endauth
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </header>
    <!-- header-end -->
    @yield('content')
    
    <!-- footer start -->
    <footer class="footer">
        <div class="footer_top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-md-6 col-lg-4">
                        <div class="footer_widget">
                            <div class="footer_logo">
                                <a href="{{route('index')}}" style="color:#fff;">
                                    {{setting('site.title')}}
                                </a>
                            </div>
                            <a href="mailto:contacto@providencialis.com" style="color:#fff;">contacto@providencialis.com</a>
                            <div class="socail_links">
                                <ul>
<!--                                    <li>
                                        <a href="#">
                                            <i class="ti-facebook"></i>
                                        </a>
                                    </li>-->
                                    <li>
                                        <a href="https://twitter.com/providencialis" target="_blank">
                                            <i class="ti-twitter-alt"></i>
                                        </a>
                                    </li>
<!--                                    <li>
                                        <a href="#">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </li>-->
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-2 offset-xl-1 col-md-6 col-lg-3">
                        
                    </div>
                    <div class="col-xl-2 col-md-6 col-lg-2">
                        
                    </div>
                    <div class="col-xl-3 col-md-6 col-lg-3">
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right_text">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/ footer end  -->

    <!-- JS here -->
    <script src="{{asset('js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <script src="{{asset('js/vendor/jquery-1.12.4.min.js')}}"></script>
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('js/ajax-form.js')}}"></script>
    <script src="{{asset('js/waypoints.min.js')}}"></script>
    <script src="{{asset('js/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{asset('js/scrollIt.js')}}"></script>
    <script src="{{asset('js/jquery.scrollUp.min.js')}}"></script>
    <script src="{{asset('js/wow.min.js')}}"></script>
    <script src="{{asset('js/nice-select.min.js')}}"></script>
    <script src="{{asset('js/jquery.slicknav.min.js')}}"></script>
    <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>
    <script src="{{asset('js/gijgo.min.js')}}"></script>

    <!--contact js-->
    <script src="{{asset('js/contact.js')}}"></script>
    <script src="{{asset('js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{asset('js/jquery.form.js')}}"></script>
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('js/jqury.valdiate.additional-methods.min.js')}}"></script>
    <script src="{{asset('js/jquery.validate_es.js')}}"></script>
    <script src="{{asset('js/mail-script.js')}}"></script>
    <script src="{{asset('vendor/select2/select2.min.js')}}"></script>
    
    <script src="{{asset('vendor/daterangepicker/moment.min.js')}}"></script>
    <script src="{{asset('vendor/daterangepicker/jquery.daterangepicker.js')}}"></script>
    <script src="{{asset('vendor/toggle/bootstrap-toggle.min.js')}}"></script>

    <script src="{{asset('js/main.js')}}"></script>
    <script>
        $(document).ready(function(){
            $("[data-toggle=popover]").popover({
                html: true, 
                      content: function() {
                      return $('#popover-content').html();
                }
            });
        });
    </script>
    @yield('js')
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-165363997-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-165363997-1');
    </script>

</body>

</html>