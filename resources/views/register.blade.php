@extends('tpl.master')

@section('content')
    <div class="bradcam_area bradcam_bg_1">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="bradcam_text">
                        <h3>{{$title}}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="contact-section">
        <div class="container">
            <div class="row">
                @include('flash-message')
                <div class="col-lg-8">
                    <form class="form-contact contact_form" action="{{route('register-save')}}" method="post" id="contactForm" novalidate="novalidate">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="name">Nombre</label>
                                    <input class="form-control" name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nombre'" placeholder="Nombre" required="">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="country">Pa&iacute;s</label>
                                    <select class="form-control" name="country" id="country" required="">
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="city">Ciudad</label>
                                    <input class="form-control" name="city" id="city" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Ciudad'" placeholder="Ciudad" required="">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="speciality">Especialidad de derecho</label>
                                    <select class="form-control" name="speciality" id="speciality" required="">
                                        @foreach($specialities as $speciality)
                                            <option value="{{$speciality->id}}">{{$speciality->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="email">Correo</label>
                                    <input class="form-control" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Correo'" placeholder="Correo" required="">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="confirm">Confirmar Correo</label>
                                    <input class="form-control" name="confirm" id="confirm" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Confirmar Correo'" placeholder="Confirmar Correo" required="">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="password">Contrase&ntilde;a</label>
                                    <input class="form-control valid" name="password" id="email" type="password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Contraseña'" placeholder="Contrase&ntilde;a" required="">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="policy">
                                        <input class="" name="policy" id="policy" type="checkbox" required="">
                                        He le&iacute;do y acepto a conformidad con <a href="{{route('terminos-condiciones')}}" target="_blank">manejo de datos personales y t&eacute;rminos y condiciones</a>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <button type="submit" class="button button-contactForm boxed-btn">Registrar</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-3 offset-lg-1"></div>
            </div>
        </div>
    </section>
@stop