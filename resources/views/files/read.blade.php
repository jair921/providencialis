@extends('tpl.master')

@section('css')
    @php
      function percentStarFiles($stars, $star){
         if($stars == 0) return 0;
         return number_format(($star*100)/$stars, 0);
      }
    @endphp
<style>
    /* Individual bars */
    .bar-5 {width: {{percentStarFiles($stars, $star5)}}%; height: 18px; background-color: #4CAF50;}
    .bar-4 {width: {{percentStarFiles($stars, $star4)}}%; height: 18px; background-color: #2196F3;}
    .bar-3 {width: {{percentStarFiles($stars, $star3)}}%; height: 18px; background-color: #00bcd4;}
    .bar-2 {width: {{percentStarFiles($stars, $star2)}}%; height: 18px; background-color: #ff9800;}
    .bar-1 {width: {{percentStarFiles($stars, $star1)}}%; height: 18px; background-color: #f44336;}
    @auth
    #rates:hover > span:before {
        color: orange;
    }
    #rates > .star-review:hover ~ .star-review:before {
        color: #c7c5c5;
    }
    @endauth
</style>
@stop

@section('content')
    @php
      function printStarFiles($avg){
         if($avg == 0){
            for($i=0; $i<5;$i++){
                echo '<span class="fa fa-star star-review" style="font-size: 25px;" data-star=""></span>'."\n";
            }
            return;
         }
         if($avg == 5){
            for($i=0; $i<5;$i++){
              echo '<span class="fa fa-star checked star-review" style="font-size: 25px;" data-star=""></span>'."\n";
            }
            return;
         }
         $avgInt = (int)$avg;
         $c=0;
         for($i=0; $i<$avgInt;$i++){
            echo '<span class="fa fa-star checked star-review" style="font-size: 25px;" data-star=""></span>'."\n";
            $c++;
          }
          if(($avg - $avgInt)>=0.5){
            echo '<span class="fa fa-star-half checked star-review half-star" style="font-size: 25px;" data-star=""></span>'."\n";
            for($i=0; $i<5-$c-1; $i++){
              echo '<span class="fa fa-star star-review" style="font-size: 25px;" data-star=""></span>'."\n";
            }
          }else{
            echo '<span class="fa fa-star star-review" style="font-size: 25px;" data-star=""></span>'."\n";
            for($i=0; $i<5-$c-1; $i++){
              echo '<span class="fa fa-star star-review" style="font-size: 25px;" data-star=""></span>'."\n";
            }
          }
      }
    @endphp
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <h3>{!!$title!!}</h3>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <div class="page-content read container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group mt-3">
                                <a href="#" onclick="window.history.back()">
                                    <button type="submit" class="button button-contactForm boxed-btn">Regresar</button>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-12" id="rates">
                            <span class="heading">Calificaciones</span>
                            {{printStarFiles($avg)}}
                            <p>{{number_format($avg, 1)}} promedio  basado en {{$stars}} calificaciones. <a href="#" id="viewDetail">Ver detalle</a></p>
                        </div>
                        
                        <div class="col-md-12" style="display:none;" id="detail">
                            <div class="panel panel-bordered">
                                <div class="side">
                                  <div>5 estrellas</div>
                                </div>
                                <div class="middle">
                                  <div class="bar-container">
                                    <div class="bar-5"></div>
                                  </div>
                                </div>
                                <div class="side right">
                                  <div>{{$star5}}</div>
                                </div>
                                <div class="side">
                                  <div>4 estrellas</div>
                                </div>
                                <div class="middle">
                                  <div class="bar-container">
                                    <div class="bar-4"></div>
                                  </div>
                                </div>
                                <div class="side right">
                                  <div>{{$star4}}</div>
                                </div>
                                <div class="side">
                                  <div>3 estrella</div>
                                </div>
                                <div class="middle">
                                  <div class="bar-container">
                                    <div class="bar-3"></div>
                                  </div>
                                </div>
                                <div class="side right">
                                  <div>{{$star3}}</div>
                                </div>
                                <div class="side">
                                  <div>2 estrellas</div>
                                </div>
                                <div class="middle">
                                  <div class="bar-container">
                                    <div class="bar-2"></div>
                                  </div>
                                </div>
                                <div class="side right">
                                  <div>{{$star2}}</div>
                                </div>
                                <div class="side">
                                  <div>1 estrella</div>
                                </div>
                                <div class="middle">
                                  <div class="bar-container">
                                    <div class="bar-1"></div>
                                  </div>
                                </div>
                                <div class="side right">
                                  <div>{{$star1}}</div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-12">

                            <div class="panel panel-bordered" style="padding-bottom:5px;">
                                <!-- form start -->
                                @foreach($dataType->readRows as $row)
                                @php
                                if ($dataTypeContent->{$row->field.'_read'}) {
                                $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_read'};
                                }
                                @endphp
                                <div class="panel-heading" style="border-bottom:0;">
                                    <h3 class="panel-title">{{ $row->getTranslatedAttribute('display_name') }}</h3>
                                </div>

                                <div class="panel-body" style="padding-top:0;">
                                    @if (isset($row->details->view))
                                    @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => 'read', 'view' => 'read', 'options' => $row->details])
                                    @elseif($row->type == "image")
                                    <img class="img-responsive"
                                         src="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">
                                    @elseif($row->type == 'multiple_images')
                                    @if(json_decode($dataTypeContent->{$row->field}))
                                    @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
                                    <img class="img-responsive"
                                         src="{{ filter_var($file, FILTER_VALIDATE_URL) ? $file : Voyager::image($file) }}">
                                    @endforeach
                                    @else
                                    <img class="img-responsive"
                                         src="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">
                                    @endif
                                    @elseif($row->type == 'relationship')
                                    @include('voyager::formfields.relationship', ['view' => 'read', 'options' => $row->details])
                                    @elseif($row->type == 'select_dropdown' && property_exists($row->details, 'options') &&
                                    !empty($row->details->options->{$dataTypeContent->{$row->field}})
                                    )
                                    <?php echo $row->details->options->{$dataTypeContent->{$row->field}}; ?>
                                    @elseif($row->type == 'select_multiple')
                                    @if(property_exists($row->details, 'relationship'))

                                    @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                                    {{ $item->{$row->field}  }}
                                    @endforeach

                                    @elseif(property_exists($row->details, 'options'))
                                    @if (!empty(json_decode($dataTypeContent->{$row->field})))
                                    @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                                    @if (@$row->details->options->{$item})
                                    {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                                    @endif
                                    @endforeach
                                    @else
                                    {{ __('voyager::generic.none') }}
                                    @endif
                                    @endif
                                    @elseif($row->type == 'date' || $row->type == 'timestamp')
                                    @if ( property_exists($row->details, 'format') && !is_null($dataTypeContent->{$row->field}) )
                                    {{ \Carbon\Carbon::parse($dataTypeContent->{$row->field})->formatLocalized($row->details->format) }}
                                    @else
                                    {{ $dataTypeContent->{$row->field} }}
                                    @endif
                                    @elseif($row->type == 'checkbox')
                                    @if(property_exists($row->details, 'on') && property_exists($row->details, 'off'))
                                    @if($dataTypeContent->{$row->field})
                                    <span class="label label-info">{{ $row->details->on }}</span>
                                    @else
                                    <span class="label label-primary">{{ $row->details->off }}</span>
                                    @endif
                                    @else
                                    {{ $dataTypeContent->{$row->field} }}
                                    @endif
                                    @elseif($row->type == 'color')
                                    <span class="badge badge-lg" style="background-color: {{ $dataTypeContent->{$row->field} }}">{{ $dataTypeContent->{$row->field} }}</span>
                                    @elseif($row->type == 'coordinates')
                                    @include('voyager::partials.coordinates')
                                    @elseif($row->type == 'rich_text_box')
                                    @include('voyager::multilingual.input-hidden-bread-read')
                                    {!! $dataTypeContent->{$row->field} !!}
                                    @elseif($row->type == 'file')
                                    @if(json_decode($dataTypeContent->{$row->field}))
                                    @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
                                    <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}">
                                        {{ $file->original_name ?: '' }}
                                    </a>
                                    <br/>
                                    @endforeach
                                    @else
                                    <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($row->field) ?: '' }}">
                                        {{ __('voyager::generic.download') }}
                                    </a>
                                    @endif
                                    @else
                                    @include('voyager::multilingual.input-hidden-bread-read')
                                    <p>{{ $dataTypeContent->{$row->field} }}</p>
                                    @endif
                                </div><!-- panel-body -->
                                @if(!$loop->last)
                                <hr style="margin:0;">
                                @endif
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-3 offset-lg-1"></div>
        </div>
    </div>
</section>
@stop

@section('js')
<script>
    $(document).ready(function(){
        $("#viewDetail").click(function(){
            
            $("#detail").toggle("slow");
            
            return false;
        });
        
        @auth
        $(".half-star").hover(function(){
            $(this).addClass("fa-star");
            $(this).removeClass("fa-star-half");
        }, function(){
            $(this).addClass("fa-star-half");
            $(this).removeClass("fa-star");
        });
        
        var star=1;
        $(".star-review").each(function(){
            $(this).data("star", star);
            star++;
        });
        $(".star-review").click(function(){
            var review = $(this).data("star");
            @php
              if($dataTypeContent->type_control == "c"){
                  $url = route("starC");
              }else{
                  $url = route("starA");
              }
            @endphp
            $.get("{{$url}}", {star:review, id:{{$dataTypeContent->id}}}, function(e){
                window.location.reload();
            });
        });
        @endauth
    });
</script>
@stop