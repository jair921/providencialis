<table>
    <thead>
        <tr>
            <th>Nombre de la ficha</th>
            <th>Autoridad Judicial</th>
            <th>Número de la sentencia</th>
            <th>Magistrado ponente</th>
            <th>Tema</th>
            <th>Accionante</th>
            <th>Accionado</th>
            <th>Tipo de proceso</th>
            <th>Hechos</th>
            <th>Problema Jurídico</th>
            <th>Respuesta al problema jurídico (Regla)</th>
            <th>Ratio Decidendi (Extractos de la sentencia)</th>
            <th>Tipo de decisión</th>
            <th>Contenido de la decisión</th>
            <th>Fecha de creación</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $row)
        <tr>
            <td>{{strip_tags($row->name)}}</td>
            <td>{{strip_tags(\App\Authority::find($row->authority_id)->name)}}</td>
            <td>{{strip_tags($row->judgment)}}</td>
            <td>{{strip_tags($row->magistrate)}}</td>
            <td>{{strip_tags($row->topic)}}</td>
            <td>{{strip_tags($row->actuator)}}</td>
            <td>{{strip_tags($row->actuated)}}</td>
            <td>{{strip_tags(\App\TypeProcess::find($row->type_id)->name)}}</td>
            <td>{{strip_tags($row->acts)}}</td>
            <td>{{strip_tags($row->problem)}}</td>
            <td>{{strip_tags($row->answer)}}</td>
            <td>{{strip_tags($row->ratio_decidendi)}}</td>
            <td>{{strip_tags(\App\DecisionType::find($row->decision_type_id)->name)}}</td>
            <td>{{strip_tags($row->content_decision)}}</td>
            <td>{{strip_tags($row->created_at)}}</td>
        </tr>
        @endforeach
    </tbody>
</table>