@php
  $dRow = \App\DataRow::where('data_type_id', 6)->where('field', 'decision_type_id')->first();
  $decisionAbs = \json_decode($dRow->details, true);
    
  $dRow2 = \App\DataRow::where('data_type_id', 6)->where('field', 'authority_id')->first();
  $authority = \json_decode($dRow2->details, true);
@endphp
<table>
    <thead>
        <tr>
            <th>Nombre de la ficha</th>
            <th>Autoridad Judicial</th>
            <th>Número de la sentencia</th>
            <th>Magistrado ponente</th>
            <th>Tema</th>
            <th>Accionante</th>
            <th>Norma Demandada</th>
            <th>Problema Jurídico</th>
            <th>Respuesta al problema jurídico (Regla)</th>
            <th>Ratio Decidendi (Extractos de la sentencia)</th>
            <th>Obiter Dicta (Extractos de la setencia)</th>
            <th>Tipo de decisión</th>
            <th>Contenido de la decisión</th>
            <th>Fecha de creación</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $row)
        <tr>
            <td>{{strip_tags($row->name)}}</td>
            <td>{{strip_tags($authority['options'][$row->authority_id])}}</td>
            <td>{{strip_tags($row->judgment)}}</td>
            <td>{{strip_tags($row->magistrate)}}</td>
            <td>{{strip_tags($row->topic)}}</td>
            <td>{{strip_tags($row->actuator)}}</td>
            <td>{{strip_tags($row->law)}}</td>
            <td>{{strip_tags($row->problem)}}</td>
            <td>{{strip_tags($row->answer)}}</td>
            <td>{{strip_tags($row->ratio_decidendi)}}</td>
            <td>{{strip_tags($row->obiter_dicta)}}</td>
            <td>{{strip_tags($decisionAbs['options'][$row->decision_type_id])}}</td>
            <td>{{strip_tags($row->content_decision)}}</td>
            <td>{{strip_tags($row->created_at)}}</td>
        </tr>
        @endforeach
    </tbody>
</table>