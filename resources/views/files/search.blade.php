@extends('tpl.master')

@section('content')
    @php
      function printStarFiles($avg){
         if($avg == 0){
            for($i=0; $i<5;$i++){
                echo '<span class="fa fa-star star-review" style="font-size: 15px;" data-star="" title="'. number_format($avg, 1) .'"></span>'."\n";
            }
            return;
         }
         if($avg == 5){
            for($i=0; $i<5;$i++){
              echo '<span class="fa fa-star checked star-review" style="font-size: 15px;" data-star="" title="'. number_format($avg, 1) .'"></span>'."\n";
            }
            return;
         }
         $avgInt = (int)$avg;
         $c=0;
         for($i=0; $i<$avgInt;$i++){
            echo '<span class="fa fa-star checked star-review" style="font-size: 15px;" data-star="" title="'. number_format($avg, 1) .'"></span>'."\n";
            $c++;
          }
          if(($avg - $avgInt)>=0.5){
            echo '<span class="fa fa-star-half checked star-review half-star" style="font-size: 15px;" data-star="" title="'. number_format($avg, 1) .'"></span>'."\n";
            for($i=0; $i<5-$c-1; $i++){
              echo '<span class="fa fa-star star-review" style="font-size: 15px;" data-star="" title="'. number_format($avg, 1) .'"></span>'."\n";
            }
          }else{
            echo '<span class="fa fa-star star-review" style="font-size: 15px;" data-star="" title="'. number_format($avg, 1) .'"></span>'."\n";
            for($i=0; $i<5-$c-1; $i++){
              echo '<span class="fa fa-star star-review" style="font-size: 15px;" data-star="" title="'. number_format($avg, 1) .'"></span>'."\n";
            }
          }
      }
    @endphp
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <h3>{!!$title!!}</h3>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="contact-section">
        <div class="container">
            <div class="row">
                @include('flash-message')
                <div class="col-lg-8">
                    <form class="form-contact contact_form" action="{{route('searchFile')}}" method="get" id="contactForm" >
                        <div class="row">
                            
                           
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="authority">Autoridad Judicial</label>
                                    <select class="form-control" name="authority" id="authority" >
                                        <option value=""></option>
                                        @foreach($authorities as $authority)
                                            <option value="{{$authority->id}}" {{$request->authority==$authority->id ? 'selected' : ''}}>{{$authority->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="judgment">N&uacute;mero de sentencia</label>
                                    <input type="text" class="form-control" name="judgment" id="judgment" value="{{$request->judgment}}">
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="magistrate">Magistrado Ponente</label>
                                    <input type="text" class="form-control" name="magistrate" id="magistrate" value="{{$request->magistrate}}">
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="search">B&uacute;squeda general</label>
                                    <input type="text" class="form-control" name="search" id="search" value="{{$request->search}}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <button type="submit" class="button button-contactForm boxed-btn">Buscar</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-3 offset-lg-1"></div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-bordered">
                                <div class="panel-body">

                                    <div class="table-responsive">
                                        <table id="dataTable" class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <a href="">Autoridad Judicial</a>
                                                    </th>
                                                    <th>
                                                        <a href="">N&uacute;mero de sentencia</a>
                                                    </th>
                                                    <th>
                                                        <a href="">Magistrado ponente</a>
                                                        </th>
                                                    <th>
                                                        <a href="">Fecha de creaci&oacute;n</a>
                                                    </th>
                                                    <th>
                                                        <a href="">Valoraci&oacute;n promedio</a>
                                                    </th>
                                                    <th class="actions text-right">Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(count($query)>0)
                                                    @foreach($query as $row)
                                                    <tr>
                                                        <td>
                                                            @if($row->type_control=='c')
                                                               @php
                                                                 $at = \App\Authority::find($row->authority_id);
                                                               @endphp
                                                               {{$at->name}}
                                                            @else
                                                              {{$authorityAbs['options'][$row->authority_id] }}
                                                            @endif
                                                        </td>
                                                        <td>{{$row->judgment}}</td>
                                                        <td>{{$row->magistrate}}</td>
                                                        <td>{{$row->created_at}}</td>
                                                        <td>
                                                            @php
                                                              if($row->type_control=='c'){
                                                                $avg = \App\ReviewFile::where('file_id', $row->id)->avg('star');
                                                              }else{
                                                                $avg = \App\ReviewFile::where('file_id', $row->id)->avg('star'); 
                                                              }
                                                            @endphp
                                                            {{printStarFiles($avg)}}
                                                        </td>
                                                        @php
                                                          if($row->type_control=='c')
                                                             $dataType = $dataType1;
                                                          else
                                                             $dataType = $dataType2;

                                                             $data = $row;
                                                        @endphp
                                                        <td class="no-sort no-click bread-actions">
                                                            @foreach($actions as $action)
                                                                @if (!method_exists($action, 'massAction'))
                                                                    @php
                                                                        // need to recreate object because policy might depend on record data
                                                                        $class = get_class($action);
                                                                        $action = new $class($dataType, $data);
                                                                    @endphp
                                                                    <a href="{{ $action->getRoute($dataType->name) }}" title="{{ $action->getTitle() }}" {!! $action->convertAttributesToHtml() !!}>
                                                                        <i class="{{ $action->getIcon() }}"></i> <span class="hidden-xs hidden-sm">{{ $action->getTitle() }}</span>
                                                                    </a>
                                                                @endif
                                                            @endforeach
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                @else
                                                <tr>
                                                    <td colspan="8">No hay fichas para mostrar</td>
                                                </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                    <div class="pull-left">
                                        <div role="status" class="show-res" aria-live="polite">{{ trans_choice(
                                            'voyager::generic.showing_entries', $total, [
                                                'from' => $from,
                                                'to' => $to,
                                                'all' => $total
                                            ]) }}</div>
                                    </div>
                                    <div class="pull-right">
                                        {{ $query->links() }}
                                    </div>
                                    
                                </div>
                    </div>
                </div>
            </div>
            @if(count($query)>0)
            <div class="row">
                <div class="form-group mt-3">
                    <a href="{{route('export.excel').($qExport ? '?'.$qExport : '')}}">
                        <button type="submit" class="button button-contactForm boxed-btn">Exportar a Excel</button>
                    </a>
                </div>
                <div class="form-group col-md-1"></div>
                <div class="form-group mt-3">
                    <a href="{{route('export.word').($qExport ? '?'.$qExport : '')}}">
                        <button type="submit" class="button button-contactForm boxed-btn">Exportar a Word</button>
                    </a>
                </div>
            </div>
            @endif
        </div>
    </section>

@stop

@section('js')
    <script>
        $(document).ready(function(){
            var decisions = {!!$jDecision!!};
            $("#type").change(function(){
                var val = $(this).val();
                var options="<option value=\"\"></option>";
                if(val == "1"){
                    $.each(decisions.concreto, function(i, v){
                        options+="<option value=\""+i+"\">"+v+"</option>";
                    });
                    $("#typeD").html(options);
                }else if(val == "2"){
                    $.each(decisions.abstracto, function(i, v){
                        options+="<option value=\""+i+"\">"+v+"</option>";
                    });
                    $("#typeD").html(options);
                }else{
                    $.each(decisions.concreto, function(i, v){
                        options+="<option value=\""+i+"\">"+v+"</option>";
                    });
                    $.each(decisions.abstracto, function(i, v){
                        options+="<option value=\""+i+"\">"+v+"</option>";
                    });
                    $("#typeD").html(options);
                }
            });
        });
    </script>
@stop