@extends('tpl.master')

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
@stop

@section('content')
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <h3>{!!$title!!}</h3>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="contact-section">
    <div class="container">
        <div class="row">
            @include('flash-message')
            <div class="col-lg-8" id="modelo">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="email">Modelo</label>
                            <select class="form-control" id="modeloSelect">
                                <option value="1" @if(Session::has('typeControl') && Session::get('typeControl')=='1') selected="" @endif>Ficha de sentencia de control Concreto</option>
                                <option value="2" @if(Session::has('typeControl') && Session::get('typeControl')=='2') selected="" @endif>Ficha de sentencia de control Abstracto</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9" id="concreto">
                @include('files.edit-add')
            </div>
            <div class="col-lg-9" id="abstracto" >
                @include('files.edit-add-a')
            </div>
            <div class="col-lg-3 offset-lg-1"></div>
        </div>
    </div>
</section>
@stop

@section('js')
    <script>
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
          return function() {
            $file = $(this).siblings(tag);

            params = {
                slug:   '{{ $dataType->slug }}',
                filename:  $file.data('file-name'),
                id:     $file.data('id'),
                field:  $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
          };
        }

        $('document').ready(function () {
//            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: [ 'YYYY-MM-DD' ]
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>


<script>
    $(document).ready(function () {
        $('select.select2').select2({width: '100%'});
        $('select.select2-ajax').each(function () {
            $(this).select2({
                width: '100%',
                ajax: {
                    url: $(this).data('get-items-route'),
                    data: function (params) {
                        var query = {
                            search: params.term,
                            type: $(this).data('get-items-field'),
                            method: $(this).data('method'),
                            id: $(this).data('id'),
                            page: params.page || 1
                        }
                        return query;
                    }
                }
            });
            $(this).on('select2:select', function (e) {
                var data = e.params.data;
                if (data.id == '') {
                    // "None" was selected. Clear all selected options
                    $(this).val([]).trigger('change');
                } else {
                    $(e.currentTarget).find("option[value='" + data.id + "']").attr('selected', 'selected');
                }
            });

            $(this).on('select2:unselect', function (e) {
                var data = e.params.data;
                $(e.currentTarget).find("option[value='" + data.id + "']").attr('selected', false);
            });
        });
    });
</script>

<script>
    $(document).ready(function(){
        var val = $("#modeloSelect").val();
        if(val == "1"){
            $("#abstracto").hide("slow");
            $("#concreto").show("slow");
        }else{
            $("#concreto").hide("slow");
            $("#abstracto").show("slow");
        }
        $("#modeloSelect").change(function(){
            var val = $(this).val();
            if(val == "1"){
                $("#abstracto").hide("slow");
                $("#concreto").show("slow");
            }else{
                $("#concreto").hide("slow");
                $("#abstracto").show("slow");
            }
        });
    });
</script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
<script>
    $(document).ready(function() {
        $(".richTextBox").each(function(){
             $(this).summernote({
                toolbar: [
                            ['style', ['style']],
                            ['font', ['bold', 'italic', 'underline', 'clear']],
                            // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                            ['fontname', ['fontname']],
                            ['fontsize', ['fontsize']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['para', ['left', 'center', 'right', 'justify']]
                            ['table', ['table']],
                            ['insert', ['link', 'hr']],
                            ['view', ['fullscreen']]
              ],
              height: 200
            });
            $(this).summernote('fontName', 'Times New Roman');
            $(this).summernote('fontSize', 14);
        });
      });
</script>
@stop