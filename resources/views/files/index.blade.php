@extends('tpl.master')

@section('content')
<div class="bradcam_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text">
                    <h3>{!!$title!!}</h3>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="contact-section">
    <div class="container">
        <div class="row">
                <div class="col-lg-8">
                    <form class="form-contact contact_form" action="{{route('myFiles')}}" method="get" id="contactForm" >
                        <div class="row">
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="authority">Autoridad Judicial</label>
                                    <select class="form-control" name="authority" id="authority" >
                                        <option value=""></option>
                                        @foreach($authorities as $authority)
                                            <option value="{{$authority->id}}" {{$request->authority==$authority->id ? 'selected' : ''}}>{{$authority->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="judgment">N&uacute;mero de sentencia</label>
                                    <input type="text" class="form-control" name="judgment" id="judgment" value="{{$request->judgment}}">
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="magistrate">Magistrado Ponente</label>
                                    <input type="text" class="form-control" name="magistrate" id="magistrate" value="{{$request->magistrate}}">
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="search">B&uacute;squeda general</label>
                                    <input type="text" class="form-control" name="search" id="search" value="{{$request->search}}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <button type="submit" class="button button-contactForm boxed-btn">Buscar</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-3 offset-lg-1"></div>
            </div>
        <div class="row">
            @include('flash-message')
            <div class="col-lg-12">
                <div class="page-content browse container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-bordered">
                                <div class="panel-body">

                                    <div class="table-responsive">
                                        <table id="dataTable" class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <a href="">Nombre personalizado de la ficha</a>
                                                    </th>
                                                    <th>
                                                        <a href="">Autoridad Judicial</a>
                                                    </th>
                                                    <th>
                                                        <a href="">N&uacute;mero de sentencia</a>
                                                    </th>
                                                    <th>
                                                        <a href="">Magistrado ponente</a>
                                                    </th>
                                                    <th>
                                                        <a href="">Fecha de creaci&oacute;n</a>
                                                    </th>
                                                    <th>
                                                        <a href="">Visibilidad</a>
                                                    </th>
                                                    <th class="actions text-right">Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(count($query)>0)
                                                    @foreach($query as $row)
                                                    <tr>
                                                        <td>{{$row->name}}</td>
                                                        <td>
                                                            @if($row->type_control=='c')
                                                               @php
                                                                 $at = \App\Authority::find($row->authority_id);
                                                               @endphp
                                                               {{$at->name}}
                                                            @else
                                                              {{$authorityAbs['options'][$row->authority_id] }}
                                                            @endif
                                                        </td>
                                                        <td>{{$row->judgment}}</td>
                                                        <td>{{$row->magistrate}}</td>
                                                        <td>{{$row->created_at}}</td>
                                                        <td>{!!$row->view=="1" ? 'P&uacute;blico' : 'Privado'!!}</td>
                                                        @php
                                                          if($row->type_control=='c')
                                                             $dataType = $dataType1;
                                                          else
                                                             $dataType = $dataType2;

                                                             $data = $row;
                                                        @endphp
                                                        <td class="no-sort no-click bread-actions">
                                                        @foreach($actions as $action)
                                                            @if (!method_exists($action, 'massAction'))
                                                                @php
                                                                    // need to recreate object because policy might depend on record data
                                                                    $class = get_class($action);
                                                                    $action = new $class($dataType, $data);
                                                                @endphp
                                                                <a href="{{ $action->getRoute($dataType->name) }}" title="{{ $action->getTitle() }}" {!! $action->convertAttributesToHtml() !!}>
                                                                    <i class="{{ $action->getIcon() }}"></i> <span class="hidden-xs hidden-sm">{{ $action->getTitle() }}</span>
                                                                </a>
                                                                @endif
                                                        @endforeach
                                                    </td>
                                                    </tr>
                                                    @endforeach
                                                @else
                                                <tr>
                                                    <td colspan="8">No hay fichas creadas</td>
                                                </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                    <div class="pull-left">
                                        <div role="status" class="show-res" aria-live="polite">{{ trans_choice(
                                            'voyager::generic.showing_entries', $total, [
                                                'from' => $from,
                                                'to' => $to,
                                                'all' => $total
                                            ]) }}</div>
                                    </div>
                                    <div class="pull-right">
                                        {{ $query->links() }}
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 offset-lg-1"></div>
        </div>
        @if(count($query)>0)
            <div class="row">
                <div class="form-group mt-3">
                    <a href="{{route('export.excel').($qExport ? '?'.$qExport : '')}}">
                        <button type="submit" class="button button-contactForm boxed-btn">Exportar a Excel</button>
                    </a>
                </div>
                <div class="form-group col-md-1"></div>
                <div class="form-group mt-3">
                    <a href="{{route('export.word').($qExport ? '?'.$qExport : '')}}">
                        <button type="submit" class="button button-contactForm boxed-btn">Exportar a Word</button>
                    </a>
                </div>
            </div>
            @endif
    </div>
</section>
    @if(count($query))
    {{-- Single delete modal --}}
        <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->getTranslatedAttribute('display_name_singular')) }}?</h4>
                    </div>
                    <div class="modal-footer">
                        <form action="#" id="delete_form" method="POST">

                            {{ csrf_field() }}
                            <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                        </form>
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endif
@stop

@section('js')
    <script>
        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = $(this).data("url");
            $('#delete_modal').modal('show');
        });
    </script>
@stop
