<table>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Correo Electrónico</th>
            <th>Especialdiad de derecho</th>
            <th>País</th>
            <th>Ciudad</th>
            <th>Fecha creación</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $user)
        <tr>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
               @php
                  $s = \App\Specialty::find($user->speciality_id);
                  if($s){
                     $esp = $s->name;
                  }else{
                     $esp = '';
                  }
               @endphp
            <td>{{$esp}}</td>
               @php
                  $s2 = \App\Country::find($user->country_id);
                  if($s2){
                     $country = $s2->name;
                  }else{
                     $country = '';
                  }
               @endphp            
            <td>{{$country}}</td>
            <td>{{$user->city}}</td>
            <td>{{$user->created_at}}</td>
        </tr>
        @endforeach
    </tbody>
</table>