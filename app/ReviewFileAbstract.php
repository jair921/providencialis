<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ReviewFileAbstract extends Model
{
    public $table = 'review_files_abstracts';
}
