<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ExportExcel implements WithMultipleSheets{
    
    use Exportable;
    
    private $concreto;
    private $abstracto;
    private $ig;
    
    public function __construct($concreto, $abstracto, $ig = null){
        $this->concreto = $concreto;
        $this->abstracto = $abstracto;
        $this->ig = $ig;
    }
    
    public function sheets(): array
    {
        $sheets = [];
        
        if($this->ig == "1"){
            $sheets[] = new \App\Exports\Models\Concreto($this->concreto);
        }elseif($this->ig == "2"){
            $sheets[] = new \App\Exports\Models\Abstracto($this->abstracto);
        }else{
            $sheets[] = new \App\Exports\Models\Concreto($this->concreto);
            $sheets[] = new \App\Exports\Models\Abstracto($this->abstracto);
        }
        
        
        
        return $sheets;
    }
    
}
