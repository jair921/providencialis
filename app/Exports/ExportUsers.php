<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ExportUsers implements WithMultipleSheets{
    
    use Exportable;
    
    private $users;
    
    public function __construct($users){
        $this->users = $users;
    }
    
    public function sheets(): array
    {
        $sheets[] = new \App\Exports\Models\Users($this->users);
        return $sheets;
    }
    
}
