<?php

namespace App\Exports\Models;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class Abstracto implements FromView, WithTitle{
    
    private $q;
    
    public function __construct($q) {
        $this->q = $q;
    }
    
    public function collection(){
        return $this->q->get();
    }
    
    public function view(): View
    {
        return view('files.export.excel.abstracto', [
            'data' => $this->collection()
        ]);
    }

    public function title(): String {
        return "Abstracto";
    }

}