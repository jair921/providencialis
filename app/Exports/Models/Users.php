<?php

namespace App\Exports\Models;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class Users implements FromView, WithTitle{
    
    private $q;
    
    public function __construct($q) {
        $this->q = $q;
    }
    
    public function collection(){
        return $this->q->get();
    }
    
    public function view(): View
    {
        return view('users.export', [
            'data' => $this->collection()
        ]);
    }

    public function title(): String {
        return "Usuarios";
    }

}