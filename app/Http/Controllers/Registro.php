<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class Registro extends Controller
{
    public function index(){
        
        $title = 'Registro de Nuevo usuario';
        $countries = \App\Country::all();
        $specialities = \App\Specialty::where('status', 1)->get();
        
        return view('register', compact('title', 'countries', 'specialities'));
    }
    
    public function save(Request $request){
        
        $validatedData = $request->validate([
            'name' => 'required|max:200',
            'country' => 'required',
            'city' => 'required|max:200',
            'speciality' => 'required',
            'city' => 'required|max:200',
            'email' => 'required|email|max:200|unique:App\User,email',
            'confirm' => 'required|email|max:200|same:email',
            'password' => 'required|min:5',
            'policy' => 'required'
        ]);
        
        $user = new \App\User();
        
        $user->role_id = 2;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->speciality_id = $request->speciality;
        $user->country_id = $request->country;
        $user->city = $request->city;
        
        $save = $user->save();
        
        if($save){
            Auth::login($user);
            
            return redirect()->route('voyager.dashboard')
                    ->with('success', 'Registro éxitoso.');
        }
        
        return redirect()->route('register')
                ->with('error','No se pudo terminar el registro, pro favor intente de nuevo.');
        
    }
    
    public function login(){
        
        $title = 'Inicio sesi&oacute;n';
        
        return view('login', compact('title'));
    }
}
