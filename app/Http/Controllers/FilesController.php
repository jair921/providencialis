<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use TCG\Voyager\Database\Schema\SchemaManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Exports\ExportExcel;

class FilesController extends \TCG\Voyager\Http\Controllers\Controller
{
    
    use BreadRelationshipParser;
    
    public function indexMult(Request $request){
        
         $title = 'Fichas creadas';
         
         $types = \App\TypeProcess::all();
        $authorities = \App\Authority::all();
        $decisions = \App\DecisionType::all();
        
        $dRow = \App\DataRow::where('data_type_id', 6)->where('field', 'decision_type_id')->first();
         if($dRow){
             $decisionAbs = \json_decode($dRow->details, true);
         }else{
             $decisionAbs['options'] = [];
         }
         
        $dRow = \App\DataRow::where('data_type_id', 6)->where('field', 'authority_id')->first();
         if($dRow){
             $authorityAbs = \json_decode($dRow->details, true);
         }else{
             $authorityAbs['options'] = [];
         }
         
         $jDecision = [];
         foreach($decisions as $decision){
            $jDecision['concreto'][$decision->id] = $decision->name; 
         }
         
         foreach ($decisionAbs['options'] as $id=>$decision){
             $jDecision['abstracto'][$id] = $decision;
         }
         
         $jDecision = json_encode($jDecision);
         
         $queryC = \App\File::where('created_by', auth()->user()->id)
                   ->select(DB::raw('id,
                                    name,
                                    type_control,
                                    view,
                                    authority_id,
                                    magistrate,
                                    decision_type_id,
                                    judgment,
                                    created_at'))
                   ->orderBy('created_at', 'desc');
         
         $query = \App\FilesAbstract::where('created_by', auth()->user()->id)
                    ->select(DB::raw('id,
                                    name,
                                    type_control,
                                    view,
                                    authority_id,
                                    magistrate,
                                    decision_type_id,
                                    judgment,
                                    created_at'))
                   ->orderBy('created_at', 'desc')
                    ;
         
         $qs=[];
         if(isset($request->type) && $request->type == "1"){
             $queryC->where('type_control', 'c');
             $query->where('type_control', 'c');
             $qs[]="type=".$request->type;
         }
         if(isset($request->type) && $request->type == "2"){
             $queryC->where('type_control', 'a');
             $query->where('type_control', 'a');
             $qs[]="type=".$request->type;
         }
         if(isset($request->authority) && $request->authority != ""){
             $queryC->where('authority_id', $request->authority);
             $query->where('authority_id', $request->authority);
             $qs[]="authority=".$request->authority;
         }
         if(isset($request->magistrate) && $request->magistrate != ""){
             $queryC->whereRaw('lower(magistrate) like \'%'.strtolower($request->magistrate).'%\'');
             $query->whereRaw('lower(magistrate) like \'%'.strtolower($request->magistrate).'%\'');
             $qs[]="magistrate=".$request->magistrate;
         }
         if(isset($request->judgment) && $request->judgment != ""){
             $queryC->where('judgment', $request->judgment);
             $query->where('judgment', $request->judgment);
             $qs[]="judgment=".$request->judgment;
         }
         if(isset($request->search) && $request->search != ""){
             
             $qs[]="search=".$request->search;
             
            $queryC->where(function($q1) use($request){
                $columns = ['name', 'judgment', 'magistrate', 'topic', 'actuator', 'actuated', 'acts', 'problem', 'answer', 'ratio_decidendi', 'content_decision'];
                foreach($columns as $column){
                    $q1->orWhereRaw('lower('.$column.') like \'%'.strtolower($request->search).'%\'');
                }
            });
            
            $query->where(function($q2)use($request){
                $columns = ['name', 'judgment', 'magistrate', 'topic', 'actuator', 'law', 'problem', 'answer', 'ratio_decidendi', 'content_decision', 'obiter_dicta'];
                foreach($columns as $column){
                    $q2->orWhereRaw('lower('.$column.') like \'%'.strtolower($request->search).'%\'');
                }
            });
         }
          $query->unionAll($queryC);
         $q = $query->toSql();
         
         $query = $t = DB::table( DB::raw(" ( $q ) t") )
                 ->mergeBindings($query->getQuery())
                 ->orderBy('created_at', 'desc')->paginate(20);
         
         $total = $t->count();
         
         if(isset($request->page)){
             $from = ($request->page*20)+1;
             $to = ($total*$request->page)+1;
         }else{
             $from = $total ? 1 : 0;
             $to = $total;
         }
         
         $dRow = \App\DataRow::where('data_type_id', 6)->where('field', 'authority_id')->first();
         if($dRow){
             $authority = \json_decode($dRow->details);
         }else{
             $authority = [];
         }
         
         $dRow = \App\DataRow::where('data_type_id', 6)->where('field', 'decision_type_id')->first();
         if($dRow){
             $decision = \json_decode($dRow->details);
         }else{
             $decision = [];
         }
         
         $actionsLst = [\App\Actions\ViewAction::class, \App\Actions\DeleteAction::class, \App\Actions\EditAction::class, \App\Actions\ViewSearchExcelAction::class, \App\Actions\ViewSearchWordAction::class];
        
        $dataType = Voyager::model('DataType')->where('slug', '=', 'files')->first();
         
        // Actions
        $actions = [];
        if (!empty($query)) {
            foreach ($actionsLst as $action) {
                $action = new $action($dataType, null);

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }
        
        $dataType1 = Voyager::model('DataType')->where('slug', '=', 'files')->first();
        $dataType2 = Voyager::model('DataType')->where('slug', '=', 'files-abstract')->first();
        
        $qExport = implode("&", $qs);
      
        return Voyager::view('files.index', compact(
            'title', 
                'query', 
                'total', 
                'types', 
                'authorities', 
                'decisions', 
                'decisionAbs', 
                'authorityAbs', 
                'jDecision', 
                'to', 'from', 'authority', 'decision', 'actions', 'dataType1', 'dataType2', 'qExport', 'request'
        ));
        
    }
    
    public function index(Request $request){
                // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = 'files';

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                
                if(in_array($key, ['id', 'updated_at', 'deleted_at', 'created_by'])){
                    continue;
                }
                
                $field = $dataRow->where('field', $value)->first();
                $displayName = ucwords(str_replace('_', ' ', $value));
                if ($field !== null) {
                    $displayName = $field->getTranslatedAttribute('display_name');
                }
                $searchNames[$value] = $displayName;
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', $dataType->order_direction);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }
            
            $query->where('created_by', auth()->user()->id);

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($model);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'browse', $isModelTranslatable);

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        $actionsLst = [\App\Actions\ViewAction::class, \App\Actions\DeleteAction::class, \App\Actions\EditAction::class];
        
        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach ($actionsLst as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, $sortOrder ?? 'desc']];
        }

        $view = 'files.index';

//        if (view()->exists("voyager::$slug.browse")) {
//            $view = "voyager::$slug.browse";
//        }

//        $title = 'Mis an&aacute;lisis';
        $title = 'Fichas creadas';
        
        return Voyager::view($view, compact(
            'title',
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn'
        ));
    }
    
    public function newFile(){
        $title = 'Nuevo an&aacute;lisis';
        
        $slug = "files";

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
//        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
                            ? new $dataType->model_name()
                            : false;

        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = $row->details->width ?? 100;
        }
        
        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        
        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'add', $isModelTranslatable);
        
        $dataAbstract = $this->dataForms('files-abstract');
//        
        $dataTypeAbstract = $dataAbstract['dataType'];
        $dataTypeContentAbstract = $dataAbstract['dataTypeContent'];
        $isModelTranslatableAbstract = $dataAbstract['isModelTranslatable'];

        return view('files.fileNew', compact(
                'title', 
                'dataType', 
                'dataTypeContent', 
                'isModelTranslatable',
                'dataTypeAbstract', 
                'dataTypeContentAbstract', 
                'isModelTranslatableAbstract'
                )
        );
    }
    
    private function dataForms($slug){ //

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $dataTypeContent = (strlen($dataType->model_name) != 0)
                            ? new $dataType->model_name()
                            : false;

        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = $row->details->width ?? 100;
        }
        
        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        
        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'add', $isModelTranslatable);
        
        return compact('dataType', 'dataTypeContent', 'isModelTranslatable');
    }
    
    public function saveFile(Request $request){
        
        $slug = 'files';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        event(new BreadDataAdded($dataType, $data));

        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', $data)) {
                $redirect = redirect()->route("myFiles");
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => 'Análisis ' . __('voyager::generic.successfully_added_new'),
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
        
    }
    
    public function show(Request $request, $id)
    {
        $slug = 'files';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $isSoftDeleted = false;

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
            if ($dataTypeContent->deleted_at) {
                $isSoftDeleted = true;
            }
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
//        $this->authorize('read', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'read', $isModelTranslatable);

        $view = 'files.read';
        
        $title = 'Ficha: '.$dataTypeContent->name;
        
        $avg = \App\ReviewFile::where('file_id', $id)->avg('star');
        
        $star1 = \App\ReviewFile::where('file_id', $id)->where('star', 1)->count();
        $star2 = \App\ReviewFile::where('file_id', $id)->where('star', 2)->count();
        $star3 = \App\ReviewFile::where('file_id', $id)->where('star', 3)->count();
        $star4 = \App\ReviewFile::where('file_id', $id)->where('star', 4)->count();
        $star5 = \App\ReviewFile::where('file_id', $id)->where('star', 5)->count();

        $stars = \App\ReviewFile::where('file_id', $id)->count();
        
        
        return Voyager::view($view, compact('title', 'dataType', 'dataTypeContent', 'isModelTranslatable', 'isSoftDeleted', 'avg', 'stars', 'star1', 'star2', 'star3', 'star4', 'star5'));
    }
    
    public function edit(Request $request, $id)
    {
        $slug = 'files';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'edit', $isModelTranslatable);

        $title = 'Editar Ficha: '.$dataTypeContent->name;
        
        return Voyager::view('files.fileEdit', compact('title', 'dataType', 'dataTypeContent', 'isModelTranslatable'));
    }
    
    // POST BR(E)AD
    public function update(Request $request, $id)
    {
        $slug = 'files';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = $model->findOrFail($id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

        event(new BreadDataUpdated($dataType, $data));

        if (auth()->user()->can('browse', app($dataType->model_name))) {
            $redirect = redirect()->route("myFiles");
        } else {
            $redirect = redirect()->back();
        }

        return $redirect->with([
            'message'    => 'Análisis '.__('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }
    
     public function destroy(Request $request, $id)
    {
        $slug = 'files';

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

            // Check permission
            $this->authorize('delete', $data);

            $model = app($dataType->model_name);
            if (!($model && in_array(SoftDeletes::class, class_uses_recursive($model)))) {
                $this->cleanup($dataType, $data);
            }
        }

        $displayName = count($ids) > 1 ? $dataType->getTranslatedAttribute('display_name_plural') : $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->destroy($ids);
        $data = $res
            ? [
                'message'    => 'Análisis '.__('voyager::generic.successfully_deleted'),
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataDeleted($dataType, $data));
        }

        return redirect()->route("myFiles")->with($data);
    }
    
 /**
     * Remove translations, images and files related to a BREAD item.
     *
     * @param \Illuminate\Database\Eloquent\Model $dataType
     * @param \Illuminate\Database\Eloquent\Model $data
     *
     * @return void
     */
    protected function cleanup($dataType, $data)
    {
        // Delete Translations, if present
        if (is_bread_translatable($data)) {
            $data->deleteAttributeTranslations($data->getTranslatableAttributes());
        }

        // Delete Images
        $this->deleteBreadImages($data, $dataType->deleteRows->whereIn('type', ['image', 'multiple_images']));

        // Delete Files
        foreach ($dataType->deleteRows->where('type', 'file') as $row) {
            if (isset($data->{$row->field})) {
                foreach (json_decode($data->{$row->field}) as $file) {
                    $this->deleteFileIfExists($file->download_link);
                }
            }
        }

        // Delete media-picker files
        $dataType->rows->where('type', 'media_picker')->where('details.delete_files', true)->each(function ($row) use ($data) {
            $content = $data->{$row->field};
            if (isset($content)) {
                if (!is_array($content)) {
                    $content = json_decode($content);
                }
                if (is_array($content)) {
                    foreach ($content as $file) {
                        $this->deleteFileIfExists($file);
                    }
                } else {
                    $this->deleteFileIfExists($content);
                }
            }
        });
    }

    /**
     * Delete all images related to a BREAD item.
     *
     * @param \Illuminate\Database\Eloquent\Model $data
     * @param \Illuminate\Database\Eloquent\Model $rows
     *
     * @return void
     */
    public function deleteBreadImages($data, $rows, $single_image = null)
    {
        $imagesDeleted = false;

        foreach ($rows as $row) {
            if ($row->type == 'multiple_images') {
                $images_to_remove = json_decode($data->getOriginal($row->field), true) ?? [];
            } else {
                $images_to_remove = [$data->getOriginal($row->field)];
            }

            foreach ($images_to_remove as $image) {
                // Remove only $single_image if we are removing from bread edit
                if ($image != config('voyager.user.default_avatar') && (is_null($single_image) || $single_image == $image)) {
                    $this->deleteFileIfExists($image);
                    $imagesDeleted = true;

                    if (isset($row->details->thumbnails)) {
                        foreach ($row->details->thumbnails as $thumbnail) {
                            $ext = explode('.', $image);
                            $extension = '.'.$ext[count($ext) - 1];

                            $path = str_replace($extension, '', $image);

                            $thumb_name = $thumbnail->name;

                            $this->deleteFileIfExists($path.'-'.$thumb_name.$extension);
                        }
                    }
                }
            }
        }

        if ($imagesDeleted) {
            event(new BreadImagesDeleted($data, $rows));
        }
    }
    
    public function search(Request $request){
        
        $title = 'Buscar Fichas';
        
        $types = \App\TypeProcess::all();
        $authorities = \App\Authority::all();
        $decisions = \App\DecisionType::all();
        
        $dRow = \App\DataRow::where('data_type_id', 6)->where('field', 'decision_type_id')->first();
         if($dRow){
             $decisionAbs = \json_decode($dRow->details, true);
         }else{
             $decisionAbs['options'] = [];
         }
         
        $dRow = \App\DataRow::where('data_type_id', 6)->where('field', 'authority_id')->first();
         if($dRow){
             $authorityAbs = \json_decode($dRow->details, true);
         }else{
             $authorityAbs['options'] = [];
         }
         
         $jDecision = [];
         foreach($decisions as $decision){
            $jDecision['concreto'][$decision->id] = $decision->name; 
         }
         
         foreach ($decisionAbs['options'] as $id=>$decision){
             $jDecision['abstracto'][$id] = $decision;
         }
         
         $jDecision = json_encode($jDecision);
         
         $queryC = \App\File::select(DB::raw('id,
                                    name,
                                    type_control,
                                    view,
                                    authority_id,
                                    magistrate,
                                    decision_type_id,
                                    judgment,
                                    created_at'))->where('view', 1);
         
         $query = \App\FilesAbstract::select(DB::raw('id,
                                    name,
                                    type_control,
                                    view,
                                    authority_id,
                                    magistrate,
                                    decision_type_id,
                                    judgment,
                                    created_at'))->where('view', 1)
                    ;
         $qs=[];
         if(isset($request->type) && $request->type == "1"){
             $queryC->where('type_control', 'c');
             $query->where('type_control', 'c');
             $qs[]="type=".$request->type;
         }
         if(isset($request->type) && $request->type == "2"){
             $queryC->where('type_control', 'a');
             $query->where('type_control', 'a');
             $qs[]="type=".$request->type;
         }
         if(isset($request->authority) && $request->authority != ""){
             $queryC->where('authority_id', $request->authority);
             $query->where('authority_id', $request->authority);
             $qs[]="authority=".$request->authority;
         }
         if(isset($request->magistrate) && $request->magistrate != ""){
             $queryC->whereRaw('lower(magistrate) like \'%'.strtolower($request->magistrate).'%\'');
             $query->whereRaw('lower(magistrate) like \'%'.strtolower($request->magistrate).'%\'');
             $qs[]="magistrate=".$request->magistrate;
         }
         if(isset($request->judgment) && $request->judgment != ""){
             $queryC->where('judgment', $request->judgment);
             $query->where('judgment', $request->judgment);
             $qs[]="judgment=".$request->judgment;
         }
         if(isset($request->search) && $request->search != ""){
             
             $qs[]="search=".$request->search;

            $queryC->where(function($q1) use($request){
                $columns = ['name', 'judgment', 'magistrate', 'topic', 'actuator', 'actuated', 'acts', 'problem', 'answer', 'ratio_decidendi', 'content_decision'];
                foreach($columns as $column){
                    $q1->orWhereRaw('lower('.$column.') like \'%'.strtolower($request->search).'%\'');
                }
            });
            
            $query->where(function($q2)use($request){
                $columns = ['name', 'judgment', 'magistrate', 'topic', 'actuator', 'law', 'problem', 'answer', 'ratio_decidendi', 'content_decision', 'obiter_dicta'];
                foreach($columns as $column){
                    $q2->orWhereRaw('lower('.$column.') like \'%'.strtolower($request->search).'%\'');
                }
            });
         }
         
         $query->unionAll($queryC);
         $q = $query->toSql();
         
         $query = $t = DB::table( DB::raw(" ( $q ) t") )
                 ->mergeBindings($query->getQuery())
                 ->orderBy('created_at', 'desc')->paginate(20);
         
         $total = $t->count();
         
         if(isset($request->page)){
             $qs[]="page=".$request->page;
             $from = ($request->page*20)+1;
             $to = ($total*$request->page)+1;
         }else{
             $from = $total ? 1 : 0;
             $to = $total;
         }
         
        $dataType = Voyager::model('DataType')->where('slug', '=', 'files')->first();
        $dataType1 = Voyager::model('DataType')->where('slug', '=', 'files')->first();
        $dataType2 = Voyager::model('DataType')->where('slug', '=', 'files-abstract')->first();

        $actionsLst = [\App\Actions\ViewSearchAction::class, \App\Actions\ViewSearchExcelAction::class, \App\Actions\ViewSearchWordAction::class];
         
        // Actions
        $actions = [];
        if (!empty($query)) {
            foreach ($actionsLst as $action) {
                $action = new $action($dataType, null);

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }
        
        $qExport = implode("&", $qs);
        
        return view('files.search', compact(
                'title', 
                'types', 
                'authorities', 
                'decisions', 
                'decisionAbs', 
                'authorityAbs', 
                'jDecision', 
                'query', 
                'total', 
                'to', 
                'from','dataType', 'dataType1', 'dataType2',
                'actions', 'qExport', 'request')
        );
    }
    
    public function star(Request $request){
        
        $review = new \App\ReviewFile();
        
        $review->user_id = auth()->user()->id;
        $review->star = $request->star;
        $review->file_id = $request->id;
        
        $review->save();
        
    }
    
    public function exportExcel(Request $request){
        
        $queryC = \App\File::orderBy('created_at', 'desc'); 
        $query = \App\FilesAbstract::orderBy('created_at', 'desc');
        
        if(isset($request->type) && $request->type == "1"){
             $queryC->where('type_control', 'c');
             $query->where('type_control', 'c');
         }
         if(isset($request->type) && $request->type == "2"){
             $queryC->where('type_control', 'a');
             $query->where('type_control', 'a');
         }
         if(isset($request->id) && $request->id != ""){
             $queryC->where('id', $request->id);
             $query->where('id', $request->id);
         }
         if(isset($request->authority) && $request->authority != ""){
             $queryC->where('authority_id', $request->authority);
             $query->where('authority_id', $request->authority);
         }
         if(isset($request->magistrate) && $request->magistrate != ""){
             $queryC->whereRaw('lower(magistrate) like \'%'.strtolower($request->magistrate).'%\'');
             $query->whereRaw('lower(magistrate) like \'%'.strtolower($request->magistrate).'%\'');
         }
         if(isset($request->judgment) && $request->judgment != ""){
             $queryC->where('judgment', $request->judgment);
             $query->where('judgment', $request->judgment);
         }
         if(isset($request->search) && $request->search != ""){
             
            $queryC->where(function($q1) use($request){
                $columns = ['name', 'judgment', 'magistrate', 'topic', 'actuator', 'actuated', 'acts', 'problem', 'answer', 'ratio_decidendi', 'content_decision'];
                foreach($columns as $column){
                    $q1->orWhereRaw('lower('.$column.') like \'%'.strtolower($request->search).'%\'');
                }
            });
            
            $query->where(function($q2)use($request){
                $columns = ['name', 'judgment', 'magistrate', 'topic', 'actuator', 'law', 'problem', 'answer', 'ratio_decidendi', 'content_decision', 'obiter_dicta'];
                foreach($columns as $column){
                    $q2->orWhereRaw('lower('.$column.') like \'%'.strtolower($request->search).'%\'');
                }
            });
         }
        
        return (new ExportExcel($queryC, $query, ($request->id ? $request->type : null )))->download('modelos.xlsx');
    }
    
    public function exportWord(Request $request){
        
        $queryC = \App\File::orderBy('created_at', 'desc'); 
        $query = \App\FilesAbstract::orderBy('created_at', 'desc');
        
        if(isset($request->type) && $request->type == "1"){
             $queryC->where('type_control', 'c');
             $query->where('type_control', 'c');
         }
         if(isset($request->type) && $request->type == "2"){
             $queryC->where('type_control', 'a');
             $query->where('type_control', 'a');
         }
         if(isset($request->id) && $request->id != ""){
             $queryC->where('id', $request->id);
             $query->where('id', $request->id);
         }
         if(isset($request->authority) && $request->authority != ""){
             $queryC->where('authority_id', $request->authority);
             $query->where('authority_id', $request->authority);
         }
         if(isset($request->magistrate) && $request->magistrate != ""){
             $queryC->whereRaw('lower(magistrate) like \'%'.strtolower($request->magistrate).'%\'');
             $query->whereRaw('lower(magistrate) like \'%'.strtolower($request->magistrate).'%\'');
         }
         if(isset($request->judgment) && $request->judgment != ""){
             $queryC->where('judgment', $request->judgment);
             $query->where('judgment', $request->judgment);
         }
         if(isset($request->search) && $request->search != ""){
             
            $queryC->where(function($q1) use($request){
                $columns = ['name', 'judgment', 'magistrate', 'topic', 'actuator', 'actuated', 'acts', 'problem', 'answer', 'ratio_decidendi', 'content_decision'];
                foreach($columns as $column){
                    $q1->orWhereRaw('lower('.$column.') like \'%'.strtolower($request->search).'%\'');
                }
            });
            
            $query->where(function($q2)use($request){
                $columns = ['name', 'judgment', 'magistrate', 'topic', 'actuator', 'law', 'problem', 'answer', 'ratio_decidendi', 'content_decision', 'obiter_dicta'];
                foreach($columns as $column){
                    $q2->orWhereRaw('lower('.$column.') like \'%'.strtolower($request->search).'%\'');
                }
            });
         }
         
         $word = new \PhpOffice\PhpWord\PhpWord();
         //$section = $word->addSection(['orientation' => 'landscape']);
         
         $queryC->chunk(10, function($concretos) use (&$word, $request){
         
            if(isset($request->id) && $request->id != ""){
                $section = $word->addSection();
                $table = $section->addTable('1');
                $c = null;
                foreach($concretos as $c1){
                    $c = $c1;
                }
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Nombre de la ficha');
                $row->addCell(4000)->addText(html_entity_decode(html_entity_decode(strip_tags($c->name))));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Autoridad Judicial');
                $row->addCell(4000)->addText(html_entity_decode(html_entity_decode(strip_tags(\App\Authority::find($c->authority_id)->name))));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Número de la sentencia');
                $row->addCell(4000)->addText(html_entity_decode(html_entity_decode(strip_tags($c->judgment))));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Magistrado ponente');
                $row->addCell(4000)->addText(html_entity_decode(html_entity_decode(strip_tags($c->magistrate))));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Tema');
                $row->addCell(4000)->addText(html_entity_decode(html_entity_decode(strip_tags($c->topic))));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Accionante');
                $row->addCell(4000)->addText(html_entity_decode(html_entity_decode(strip_tags($c->actuator))));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Accionado');
                $row->addCell(4000)->addText(html_entity_decode(html_entity_decode(strip_tags($c->actuated))));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Tipo de proceso');
                $row->addCell(4000)->addText(html_entity_decode(html_entity_decode(strip_tags(\App\TypeProcess::find($c->type_id)->name))));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Hechos');
                $row->addCell(4000)->addText(html_entity_decode(html_entity_decode(strip_tags($c->acts))));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Problema Jurídico');
                $row->addCell(4000)->addText(html_entity_decode(html_entity_decode(strip_tags($c->problem))));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Respuesta al problema jurídico (Regla)');
                $row->addCell(4000)->addText(html_entity_decode(html_entity_decode(strip_tags($c->answer))));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Ratio Decidendi (Extractos de la sentencia)');
                $row->addCell(4000)->addText(html_entity_decode(html_entity_decode(strip_tags($c->ratio_decidendi))));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Tipo de decisión');
                $row->addCell(4000)->addText(html_entity_decode(html_entity_decode(strip_tags(\App\DecisionType::find($c->decision_type_id)->name))));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Contenido de la decisión');
                $row->addCell(4000)->addText(html_entity_decode(html_entity_decode(strip_tags($c->content_decision))));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Fecha de creación');
                $row->addCell(4000)->addText(html_entity_decode(html_entity_decode(strip_tags($c->created_at))));
                
            }else{
                
                $section = $word->addSection(['orientation' => 'landscape']);
                $table = $section->addTable('1');
            
                $row = $table->addRow();
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Nombre de la ficha');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Autoridad Judicial');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Número de la sentencia');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Magistrado ponente');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Tema');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Accionante');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Accionado');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Tipo de proceso');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Hechos');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Problema Jurídico');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Respuesta al problema jurídico (Regla)');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Ratio Decidendi (Extractos de la sentencia)');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Tipo de decisión');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Contenido de la decisión');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Fecha de creación');

                foreach($concretos as $c){
                    $row = $table->addRow();
                    $row->addCell(1000)->addText(strip_tags($c->name));
                    $row->addCell(1000)->addText(strip_tags(\App\Authority::find($c->authority_id)->name));
                    $row->addCell(1000)->addText(strip_tags($c->judgment));
                    $row->addCell(1000)->addText(strip_tags($c->magistrate));
                    $row->addCell(1000)->addText(strip_tags($c->topic));
                    $row->addCell(1000)->addText(strip_tags($c->actuator));
                    $row->addCell(1000)->addText(strip_tags($c->actuated));
                    $row->addCell(1000)->addText(strip_tags(\App\TypeProcess::find($c->type_id)->name));
                    $row->addCell(1000)->addText(strip_tags($c->acts));
                    $row->addCell(1000)->addText(strip_tags($c->problem));
                    $row->addCell(1000)->addText(strip_tags($c->answer));
                    $row->addCell(1000)->addText(strip_tags($c->ratio_decidendi));
                    $row->addCell(1000)->addText(strip_tags(\App\DecisionType::find($c->decision_type_id)->name));
                    $row->addCell(1000)->addText(strip_tags($c->content_decision));
                    $row->addCell(1000)->addText(strip_tags($c->created_at));
                }
             
            }
         });
         
         $dRow = \App\DataRow::where('data_type_id', 6)->where('field', 'decision_type_id')->first();
         if($dRow){
             $decisionAbs = \json_decode($dRow->details, true);
         }else{
             $decisionAbs['options'] = [];
         }
         
        $dRow = \App\DataRow::where('data_type_id', 6)->where('field', 'authority_id')->first();
         if($dRow){
             $authorityAbs = \json_decode($dRow->details, true);
         }else{
             $authorityAbs['options'] = [];
         }
         
         $query->chunk(10, function($abstractos) use (&$word, $decisionAbs, $authorityAbs, $request){
             
            
             if(isset($request->id) && $request->id != ""){
                $section = $word->addSection();
                $table = $section->addTable('1');
                $a = null;
                foreach($abstractos as $c1){
                    $a = $c1;
                }
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Nombre de la ficha');
                $row->addCell(4000)->addText(strip_tags($a->name));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Autoridad Judicial');
                $row->addCell(4000)->addText(strip_tags($authorityAbs['options'][$a->authority_id]));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Número de la sentencia');
                $row->addCell(4000)->addText(strip_tags($a->judgment));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Magistrado ponente');
                $row->addCell(4000)->addText(strip_tags($a->magistrate));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Tema');
                $row->addCell(4000)->addText(html_entity_decode(strip_tags($a->topic)));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Accionante');
                $row->addCell(4000)->addText(html_entity_decode(strip_tags($a->actuator)));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Norma Demandada');
                $row->addCell(4000)->addText(html_entity_decode(strip_tags($a->law)));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Problema Jurídico');
                $row->addCell(4000)->addText(html_entity_decode(strip_tags($a->problem)));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Respuesta al problema jurídico (Regla)');
                $row->addCell(4000)->addText(html_entity_decode(strip_tags($a->answer)));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Ratio Decidendi (Extractos de la sentencia)');
                $row->addCell(4000)->addText(html_entity_decode(strip_tags($a->ratio_decidendi)));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Tipo de decisión');
                $row->addCell(4000)->addText(html_entity_decode(strip_tags($decisionAbs['options'][$a->decision_type_id])));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Contenido de la decisión');
                $row->addCell(4000)->addText(html_entity_decode(strip_tags($a->content_decision)));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Fecha de creación');
                $row->addCell(4000)->addText(html_entity_decode(strip_tags($a->created_at)));
                
                $row = $table->addRow();
                $row->addCell(4000, array('vMerge' => 'restart'))->addText('Obiter Dicta (Extractos de la setencia)');
                $row->addCell(4000)->addText(html_entity_decode(strip_tags($a->obiter_dicta)));

                
             }else{
                $section = $word->addSection(['orientation' => 'landscape']);
                $table = $section->addTable('2');

                $row = $table->addRow();
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Nombre de la ficha');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Autoridad Judicial');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Número de la sentencia');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Magistrado ponente');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Tema');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Accionante');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Norma Demandada');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Problema Jurídico');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Respuesta al problema jurídico (Regla)');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Ratio Decidendi (Extractos de la sentencia)');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Tipo de decisión');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Contenido de la decisión');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Fecha de creación');
                $row->addCell(1000, array('vMerge' => 'restart'))->addText('Obiter Dicta (Extractos de la setencia)');

                foreach($abstractos as $a){
                    $row = $table->addRow();
                    $row->addCell(1000)->addText(html_entity_decode(strip_tags($a->name)));
                    $row->addCell(1000)->addText(html_entity_decode(strip_tags($authorityAbs['options'][$a->authority_id])));
                    $row->addCell(1000)->addText(html_entity_decode(strip_tags($a->judgment)));
                    $row->addCell(1000)->addText(html_entity_decode(strip_tags($a->magistrate)));
                    $row->addCell(1000)->addText(html_entity_decode(strip_tags($a->topic)));
                    $row->addCell(1000)->addText(html_entity_decode(strip_tags($a->actuator)));
                    $row->addCell(1000)->addText(html_entity_decode(strip_tags($a->law)));
                    $row->addCell(1000)->addText(html_entity_decode(strip_tags($a->problem)));
                    $row->addCell(1000)->addText(html_entity_decode(strip_tags($a->answer)));
                    $row->addCell(1000)->addText(html_entity_decode(strip_tags($a->ratio_decidendi)));
                    $row->addCell(1000)->addText(html_entity_decode(strip_tags($a->obiter_dicta)));
                    $row->addCell(1000)->addText(html_entity_decode(strip_tags($decisionAbs['options'][$a->decision_type_id])));
                    $row->addCell(1000)->addText(html_entity_decode(strip_tags($a->content_decision)));
                    $row->addCell(1000)->addText(html_entity_decode(strip_tags($a->created_at)));
                }
             }
             
             
         });
         
         $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($word, 'Word2007');
       
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Type: application/force-download');
        header('Content-Disposition: attachment; filename="modelos.docx');
        header('Content-Transfer-Encoding: Binary');

        try {

            $objWriter->save(('php://output'));

        } catch (Exception $e) {

        }
        
//        return response(file_get_contents(('php://output')))->withHeaders([
//           "Content-Description" => 'File Transfer',
//           "Content-Disposition" => 'attachment; filename="modelos.docx"',
//           "Content-Type" => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
//           "Content-Transfer-Encoding" => 'Binary', 
//        ]);
        
    }
}
