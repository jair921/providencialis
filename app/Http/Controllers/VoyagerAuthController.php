<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VoyagerAuthController extends \TCG\Voyager\Http\Controllers\VoyagerAuthController
{

    /*
     * Preempts $redirectTo member variable (from RedirectsUsers trait)
     */
    public function redirectTo()
    {

       if (Auth::user()->hasRole('user')) {
            return route('myFiles');
        }   
    
        return config('voyager.user.redirect', route('voyager.dashboard'));
    }

}

