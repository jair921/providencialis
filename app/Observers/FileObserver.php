<?php

namespace App\Observers;

use App\File;

class FileObserver
{
    /**
     * Listen to the $Application creating event.
     *
     * @param  Application  $Application
     * @return void
     */
    public function creating(File $file) {
        $file->created_by = auth()->user() ? auth()->user()->id : null;
    }
}
