<?php

namespace App\Observers;

use App\FilesAbstract;

class FileAbstractObserver
{
    /**
     * Listen to the $Application creating event.
     *
     * @param  FilesAbstract  $file
     * @return void
     */
    public function creating(FilesAbstract $file) {
        $file->created_by = auth()->user() ? auth()->user()->id : null;
    }
}
