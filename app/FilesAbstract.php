<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilesAbstract extends Model
{
    public $table = 'files_abstract';
}
