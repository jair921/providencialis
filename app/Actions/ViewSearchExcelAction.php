<?php

namespace App\Actions;

class ViewSearchExcelAction extends \TCG\Voyager\Actions\AbstractAction
{
    public function getTitle()
    {
        return 'Excel';
    }

    public function getIcon()
    {
        return 'fa fa-plus';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-success pull-right view',
        ];
    }

    public function getDefaultRoute()
    {
        return route('export.excel', ['id'=>$this->data->id, 'type' => $this->data->type_control=='c' ? 1 : 2]);
    }
}
