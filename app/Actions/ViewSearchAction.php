<?php

namespace App\Actions;

class ViewSearchAction extends \TCG\Voyager\Actions\AbstractAction
{
    public function getTitle()
    {
        return __('voyager::generic.view');
    }

    public function getIcon()
    {
        return 'fa fa-eye';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-warning pull-right view',
        ];
    }

    public function getDefaultRoute()
    {
        if($this->data->type_control == 'c')
            return route('myFile', $this->data->id);
        else
            return route('myFileAbstracto', $this->data->id);
    }
}
