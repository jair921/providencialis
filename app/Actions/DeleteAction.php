<?php

namespace App\Actions;

class DeleteAction extends \TCG\Voyager\Actions\AbstractAction
{
    public function getTitle()
    {
        return __('voyager::generic.delete');
    }

    public function getIcon()
    {
        return 'fa fa-trash';
    }

    public function getPolicy()
    {
        return 'delete';
    }

    public function getAttributes()
    {
        if($this->data->type_control == 'c'){
            $url = route('deleteFile', ['id' => $this->data->id]);
        }else{
            $url = route('deleteFileAbstracto', ['id' => $this->data->id]);
        }
        return [
            'class'   => 'btn btn-sm btn-danger pull-right delete',
            'data-id' => $this->data->id,
            'data-url' => $url,
            'id'      => 'delete-'.$this->data->id,
        ];
    }

    public function getDefaultRoute()
    {
        return 'javascript:;';
    }
}
