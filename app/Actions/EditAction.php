<?php

namespace App\Actions;

class EditAction extends \TCG\Voyager\Actions\AbstractAction
{
    public function getTitle()
    {
        return __('voyager::generic.edit');
    }

    public function getIcon()
    {
        return 'fa fa-edit';
    }

    public function getPolicy()
    {
        return 'edit';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary pull-right edit',
        ];
    }

    public function getDefaultRoute()
    {
        if($this->data->type_control == 'c')
            return route('editFile', $this->data->id);
        else
            return route('editFileAbstracto', $this->data->id);
    }
}
