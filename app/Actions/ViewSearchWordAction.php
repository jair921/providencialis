<?php

namespace App\Actions;

class ViewSearchWordAction extends \TCG\Voyager\Actions\AbstractAction
{
    public function getTitle()
    {
        return 'Word';
    }

    public function getIcon()
    {
        return 'fa fa-plus';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-info pull-right view',
        ];
    }

    public function getDefaultRoute()
    {
        return route('export.word', ['id'=>$this->data->id, 'type' => $this->data->type_control=='c' ? 1 : 2]);
    }
}
