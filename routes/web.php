<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return view('welcome');
})->name('index');

Route::get("registro", 'Registro@index')->name('register');
Route::post("registro", 'Registro@save')->name('register-save');


Route::get("login", 'Registro@login')->name('login');

Route::get('/terminos-y-condiciones', function () {
    $filename="POLITICA DE TRATAMIENTO DE LA INFORMACIÓN Y TERMINOS Y CONDICIONES DEL SERVICIO.pdf";

    return response()->file((__DIR__.'/../public/'.$filename), [
        'Content-Type' => 'application/pdf',
        'Content-Disposition' => 'inline; filename="'.$filename.'"'
    ]);
})->name('terminos-condiciones');

Route::middleware("auth")->get("nuevo-analisis", 'FilesController@newFile')->name('newFile');
Route::middleware("auth")->post("nuevo-analisis", 'FilesController@saveFile')->name('saveFile');
Route::middleware("auth")->post("eliminar-analisis/concreto/{id}", 'FilesController@destroy')->name('deleteFile');

Route::middleware("auth")->get("mis-analisis", 'FilesController@indexMult')->name('myFiles');
Route::get("mis-analisis/concreto/{id}", 'FilesController@show')->name('myFile');
Route::middleware("auth")->get("mis-analisis/concreto/editar/{id}", 'FilesController@edit')->name('editFile');
Route::post("mis-analisis/concreto/update/{id}", 'FilesController@update')->name('updateFile');

Route::get("mis-analisis/abstracto/{id}", 'ControlAbstracto@show')->name('myFileAbstracto');
Route::get("mis-analisis/abstracto/editar/{id}", 'ControlAbstracto@edit')->name('editFileAbstracto');
Route::post("mis-analisis/abstracto/update/{id}", 'ControlAbstracto@update')->name('updateFileAbstracto');

Route::post("eliminar-analisis/abstracto/{id}", 'ControlAbstracto@destroy')->name('deleteFileAbstracto');

Route::get("buscar", 'FilesController@search')->name('searchFile');
Route::get("export/excel", 'FilesController@exportExcel')->name('export.excel');
Route::get("export/word", 'FilesController@exportWord')->name('export.word');
Route::middleware("auth")->get("star/concreto", 'FilesController@star')->name('starC');
Route::middleware("auth")->get("star/abstracto", 'ControlAbstracto@star')->name('starA');

Route::resource('abstracto', 'ControlAbstracto');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    
    Route::post('login', '\App\Http\Controllers\VoyagerAuthController@postLogin')->name('voyager.postlogin');
    
    Route::post('logout', function(){
        Auth::logout();
        return redirect()->route('index');
    })->name('voyager.logout');
    
    Route::get('logout', function(){
        Auth::logout();
        return redirect()->route('index');
    })->name('voyager.logoutG');
    
    Route::get('users-excel', function(){
        $users = \App\User::where("id", '>', 0);
        return (new \App\Exports\ExportUsers($users))->download('usuarios.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    })->name('users.excel');
    Route::get('users-csv', function(){
        $users = \App\User::where("id", '>', 0);
        return (new \App\Exports\ExportUsers($users))->download('usuarios.csv', \Maatwebsite\Excel\Excel::CSV);
    })->name('users.csv');

});

Route::get('logout', function(){
        Auth::logout();
        return redirect()->route('index');
})->name('voyager.logoutG2');



Route::get('/home', function(){
  return  redirect()->route('myFiles');
})->name('home');
